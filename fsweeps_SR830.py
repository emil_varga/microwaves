# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: emil
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from scipy.ndimage import gaussian_filter

import sys
sys.path.append("D:\\ev")
import instruments.SR830 as SR830
from instruments.KS33210A import KS33210A
from instruments.rfsource import BNC865
from instruments.SG384 import SG384
from instruments.DS345 import DS345
from instruments.vna import VNA

def find_f0(vna_sweep):
    f = vna_sweep[:,0]
    x = vna_sweep[:,1]
    y = vna_sweep[:,2]
    r = np.abs(x + 1j*y)
    rs = gaussian_filter(r, 10)
    return f[np.argmin(rs)]

import ltwsclient as ltwsc
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

T3t = 0.7
T3_tolerance = 0.007
check_T_every = 5 #s
check_vna_every = 4 #sweeps

output_dir = '../data/microwaves/RUN20201214/filled/driven/BUZZ/T700/1/'

chunk_size = 60
df = 0.1
fmin = 3704
fmax = 4500

freqs = []
fi = fmin
while fi < fmax:
    freqs.append((fi, fi+chunk_size*df, chunk_size))
    fi += chunk_size*df

print("Measuring in ranges:")
for f in freqs:
    print("\t"+str(f))
    
amplitudes = [0, 10]
# amplitudes = np.linspace(2, 10, 10)

harmonics = [1]
# RFfreq = 5.3151788e9 #Hz. for 0.8 K
# RFfreq = 5.313964618e9 #Hz, for 0.9 K
RFfreq = 5.315713991e9 #Hz for 0.7 K
detune = 0 # Hz, detune from RFfreq by this much
RFspan = 10e6 #Hz
RFpoints = 1000
RFpower = -15 #dBm
IFBW = 300 #Hz
adjust_RF = False
#in addition to temperature check, check that the resonance is actually at RFfreq
#do not use together with adjust_RF
vna_T_check = True
RFfreq_tolerance = 1e5 # tolerance for vna T check
adjust_sensitivity = True
use_am = False

reps = 1000

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()
vna = VNA(rm, 'USB0::0x0957::0x1509::MY51200747::INSTR')

lockin = SR830.SR830(rm, 'GPIB0::1::INSTR')
lockin.lock()
lockin.coupling('ac')
lockin.set_sensitivity('20m')
lia_sens = 20e-3
lockin.set_timeconstant('300m')
lockin.set_slope('12')
lockin.set_reference('external')
lockin.set_reserve('high')
lockin.harmonic(1)
lockin.unlock()
wait_time = 1 # s

generator = KS33210A(rm, '33210A')
# generator = DS345(rm, 'GPIB0::20::INSTR')
generator.amplitude(amplitudes[0])
generator.output(True)

rf = BNC865(rm, 'BNC845')
rf.frequency(RFfreq + detune)
rf.power(RFpower)
rf.output(True)
rf.am(use_am, 0.1)
rf.am()

lo = SG384(rm, 'GPIB0::3::INSTR')
lo.lock()
lo.frequency(RFfreq + detune)
lo.unlock()
unbalanced = True
since_last_vna_check = np.inf
last_vna_timestamp = None
try:
    r = 0
    while r < reps:
        r += 1
        for lh in harmonics:
            lockin.lock()
            lockin.harmonic(lh)
            lockin.unlock()
            for A in amplitudes:
                n=0
                while n < len(freqs):
                    fr = freqs[n]
                    fs = np.linspace(fr[0], fr[1], fr[2])
                    # np.random.shuffle(fs)
                    print(fs[0])
                    xs, ys = [], []
                    fs_r = []
                    ts = []
                    T3s = []
                    
                    while True:
                        try:
                            T3i = lt.get_T3()
                        except:
                            T3i = np.nan
                        if abs(T3i - T3t) < T3_tolerance:
                            break
                        print("Waiting for T3={:.3f} to reach {:.3f}".format(T3i, T3t))
                        unbalanced = True
                        since_last_vna_check = np.inf
                        time.sleep(1)
                    print("Starting {} Hz, {} V".format(fr, A))
                    
                    if since_last_vna_check > check_vna_every:
                        print("Getting microwave sweep")
                        vna_sweep = vna.sweep_cs(RFfreq, RFspan, RFpoints, IFBW)
                        since_last_vna_check = 0
                        vna.output_off()
                    if adjust_RF:
                        R = np.sqrt(vna_sweep[:,1]**2 + vna_sweep[:,2]**2)
                        RFfreq = vna_sweep[np.argmin(R), 0]
                        print("Setting RF frequency to {}".format(RFfreq))
                        lo.lock()
                        lo.frequency(RFfreq + detune)
                        rf.frequency(RFfreq + detune)
                        lo.unlock()
                        unbalanced = True
                    
                    #check whether the peak is where we want it
                    if vna_T_check:
                        f0 = find_f0(vna_sweep)
                        if abs(f0 - RFfreq) > RFfreq_tolerance:
                            #try again if it's too far
                            print("Resonance frequency {} too far"
                                  " from expected {} (delta={})".format(f0, RFfreq, RFfreq-f0))
                            print("Trying again")
                            unbalanced = True
                            since_last_vna_check = np.inf
                            time.sleep(1)
                            continue
                        
                    
                    if use_am:
                        print("Configuring the amplitude modulation.")
                        unbalanced = True
                        if A < 0.01:
                            rf.am(False)
                        else:
                            rf.am(True, 0.1)
                    
                    timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    if since_last_vna_check == 0:
                        last_vna_timestamp = timestamp
                    generator.amplitude(A)
                    generator.frequency(fs[0]) # set the first frequency
                    fstr = generator.frequency()
                    if unbalanced:
                        print("Waiting 30 s for balance.")
                        time.sleep(30)
                    unbalanced = False
                    rmax = 0
                    t0 = time.time()
                    aborted = False
                    nans = 0
                    maxnans = 5
                    for k in range(len(fs)):
                        t = time.time()
                        if t - t0 > check_T_every:
                            t0 = t
                            try:
                                T3 = lt.get_T3()                                
                            except:
                                T3 = np.nan
                            if np.isnan(T3):
                                nans += 1
                                print("Couldn't get temperature!")
                                if nans > maxnans:
                                    print("Too many nans in temperature, aborting.")
                                    aborted = True
                                    break
                            elif abs(T3 - T3t) > T3_tolerance:
                                print("Lost temperature control, aborting and redoing.")
                                aborted = True
                                break
                            else:
                                nans = 0
                                T3s.append((t, T3))
                        print("\r{}/{}".format(k, len(fs)), end='')
                        freq = fs[k]
                        generator.frequency(freq)
                        time.sleep(wait_time)
                        lockin.lock()
                        while True:
                            x, y = lockin.get_xy()
                            r = np.sqrt(x**2 + y**2)
                            if r > rmax:
                                rmax = r
                            overload = lockin.overloadp()
                            if overload:
                                print("Lockin overloading!")
                            if adjust_sensitivity:
                                if rmax < 0.1*lia_sens or overload:
                                    print("Changing lockin sensitivity ({:.6f}/{:.6f}), overload={}".format(r, lia_sens, overload))
                                    lia_sens = lockin.auto_sens(rmax)
                                    # lockin.unlock()
                                    time.sleep(0.5)
                                    # lockin.lock()
                                    continue
                            break
                        lockin.unlock()
                        fstr = generator.frequency()
                        fs_r.append(float(fstr))
                        xs.append(x)
                        ys.append(y)
                        ts.append(t)
                    since_last_vna_check += 1
                    
                    t = time.time()
                    try:
                        T3 = lt.get_T3()
                    except:
                        T3 = np.nan
                    T3s.append((t, T3))

                    if aborted or abs(T3s[-1][1] - T3t) > T3_tolerance:
                        print("Lost temperature control, re-doing.")
                        since_last_vna_check = np.inf
                        continue
                    data = np.column_stack((np.array(fs_r), xs, ys, ts))
                    print(data.shape)
                    out = {'amplitude_rms': A, 'data': data, 'RF_freq': RFfreq+detune,
                           'temperatures': T3s, 'T3t': T3t, 'harmonic': lh,
                           'vna_sweep': vna_sweep, 'RFpower': RFpower,
                           'last_vna_sweep': last_vna_timestamp}
                    np.save(path.join(output_dir, 'FS_SR_'+timestamp), out, allow_pickle=True)
                    n+=1
                    # i+=1
                    # if i > 10:
                    #     i=0
finally:
    lockin.close()
    generator.output(False)
    generator.close()
    rf.close()
    lo.close()
    rm.close()
    vna.close()