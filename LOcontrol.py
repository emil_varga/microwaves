# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 08:27:29 2022

@author: Davis
"""

import pyvisa as visa
import numpy as np
import time

import sys
sys.path.append('D:/ev')

from instruments.rfsource import RFsource

rm = visa.ResourceManager()
lo = RFsource(rm, 'LOsource')
rf = RFsource(rm, 'RFsource')

#%%

freq = 2.30735e9
freq2 = freq
# lo.lock()
# lo.frequency(freq)
# lo.unlock()
# rf.frequency(freq)
# lo.reference('EXT', reffreq=10000000)
# lo.output(False)
# lo.phase(0.15)
# lo.phase(0)
rf.power(-6)
rf.frequency(freq)
lo.frequency(freq)

rf.am(True, sens=0.5)
rf.pm(False, sens=0.25)

#lo.frequency(freq)
#rf.frequency(freq2)

lo.refout(True)
lo.output(True)
rf.output(True)
# rf.output(False)

time.sleep(1)
print(lo.frequency(), lo.reference(), lo.phase(), lo.reflocked())
rf.am()
rf.pm()

#%%

lo.close()
rf.close()
rm.close()