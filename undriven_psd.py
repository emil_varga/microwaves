# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 16:50:03 2022

@author: emil
"""

import os
import time
import numpy as np
import pyvisa as visa

import sys
sys.path.append('C:/code/labtools')
import cryoutil as cu

sys.path.append('D:/ev')
from instruments.SR830 import SR830
from instruments.SR844 import SR844
from instruments.DS345 import DS345
from instruments.vna import VNA
from instruments.rfsource import RFsource
from instruments.DAQcard import DAQcard

base_output_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/undriven_psds/700mK/1'

rm = visa.ResourceManager(r"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\agbin\visa32.dll")
cc = cu.get_cryocon(rm)
vna = VNA(rm, "VNA")
Ilia = SR830(rm, 'GPIB0::1::INSTR')
# Qlia = SR830(rm, 'GPIB0::2::INSTR')
gen = DS345(rm, 'GPIB0::5::INSTR')
lo = RFsource(rm, 'LOsource')
rf = RFsource(rm, 'RFsource')
rfmixed = RFsource(rm, 'RFsource_mixed')

vna.power(-20)
vna.setup('S21')

# cavity_centre = 2.30689e9 #Hz 600 mK
cavity_centre = 2.3061e9 #Hz, 700 mK
cavity_sweep_span = 5e6 #Hz

# f_segments = [ #center, span, stepping
#     # (150e3, 100e3, 50),
#     (540, 100, 1),
#     (769, 20, 0.1),
#     (1184, 100, 0.1),
#     (1594, 50, 0.1),
#     (2391, 50, 0.1),
#     (4000, 2000, 1)
#     ]
# f_segments = [(2600, 5000, 0.5)]
f_segments = [(1380, 6, 0.01)]

wait_time = 1 #s

Ilia.set_timeconstant('300m')
# Qlia.set_timeconstant('300m')
Ilia.set_sensitivity('10u')
Ilia.set_slope('12')
# Qlia.set_slope('12')
Ilia.harmonic(1)
# Qlia.harmonic(1)
gen.amplitude(-20, unit='DB')
gen.offset(0)
rf.power(-5)

# RF_frequencies = [2.30689e9, 2.3064e9, 2.3074e9, 2.5e9]
RF_frequencies = [2.3061e9]
drive_offset = 0#20e3

lo_phases = [0]
gen_powers = [0, 5, 10, 20]

lo.output(True)
rf.output(True)
rfmixed.output(False)
was_warm = False
while True:
    for RF_freq in RF_frequencies:
        print("Starting RF f {:.2f}".format(RF_freq))
        data_dir = os.path.join(base_output_dir, 'RF_{:.0f}'.format(RF_freq))
        os.makedirs(data_dir, exist_ok=True)
        lo.lock()
        lo.frequency(RF_freq)
        lo.unlock()
        rf.frequency(RF_freq)
        # rfmixed.frequency(RF_freq+drive_offset)
        
        print("waiting 3 min for homdyne lock")
        time.sleep(180)
        for power in gen_powers:            
            gen.amplitude(power, unit='DB')
            print(f"Starting drive power {power} dBm.")
            
            for lo_phase in lo_phases:
                # lo.lock()
                # lo.phase(lo_phase)
                # lo.unlock()
                print("Starting phase {}".format(lo.phase()))
                while True:
                    try:
                        T3 = cu.get_T3(cc)
                        if T3 < 0.705:
                            if was_warm:
                                print("First cooldown, waiting 3 min")
                                was_warm = False
                                time.sleep(180)
                            break
                        print("T3 = {}".format(T3))
                        was_warm = True
                    except:
                        print("Couldn't read T3, trying again.")
                    time.sleep(5)
                t0 = time.time()
                print("taking vna sweep")
                # vna_sweep = vna.sweep_cs(cavity_centre, cavity_sweep_span, bw=2e3)
                # vna.output_off()
                vna_sweep = None
                
                for fc, fs, step in f_segments:
                    Is = []
                    Qs = []
                    Ts = []
                    ts = []
                    timestamp = time.strftime("%Y%m%d-%H%M%S")
                    frequencies = np.arange(fc - fs/2, fc + fs/2+step, step)
                    for f in frequencies:
                        print("Starting {} Hz".format(f))
                        gen.frequency(f)
                        time.sleep(wait_time)
                        while True:
                            try:
                                T3 = cu.get_T3(cc)
                                if T3 < 0.705:
                                    if was_warm:
                                        print("First cooldown, waiting 2 min")
                                        was_warm = False
                                        time.sleep(60)
                                    break
                                print("T3 = {}".format(T3))
                                was_warm = True
                            except:
                                print("Couldn't read T3, trying again.")
                            time.sleep(5)
                        Ilia.lock()
                        Ix, Iy = Ilia.get_xy()
                        Ilia.unlock()
                        # Qlia.lock()
                        # Qx, Qy = Qlia.get_xy()
                        # Qlia.unlock()
                        Qx, Qy = 0, 0
                        t = time.time()
                        Is.append(Ix + 1j*Iy)
                        Qs.append(Qx + 1j*Qy)
                        Ts.append(T3)
                        ts.append(t - t0)
                    
                    Is = np.array(Is)
                    Qs = np.array(Qs)
                    Ts = np.array(Ts)
                    ts = np.array(ts)
                    
                    output = {'freq': frequencies, 'I': Is, 'Q': Qs, 'T3': Ts, 'cavity': vna_sweep,
                              'time': ts, 'lo_phase': lo_phase, 'buzzer_power': power}
                    np.save(os.path.join(data_dir, 'OMIT_{}.npy'.format(timestamp)), output)