#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:01:02 2019

@author: emil
"""

import numpy as np
import visa
import ziLockin as zi
from datetime import datetime
import os.path as path
import os
import ltwsclient as ltwsc
import instruments.rfsource as rfsource

device_id = 'dev538'
reps = 5
continuous = True

output_dir = 'data_RUN20200905/mechanics/RFvariable/MECH58kHz'
# freq_i = 133.9e3 #Hz
# freq_f = 134.3e3 #Hz
freq_i = 57e3
freq_f = 58e3
samples = 1000
time_constant = 50e-3
navg = 1
RFfreqs = np.linspace(2.77e9, 2.79e9, 50)
# RFfreqs = [2.7802e9]
# As = [5, 2, 1, 0.5, 0.2, 0.1, 0]
# As = [0, 0.1, 0.2, 0.5, 1, 2, 5]
As = [0.5]
# As = np.linspace(0, 1, 10)
T3max = 0.55

print(As)

os.makedirs(output_dir,exist_ok=True)

input_channel  = 0
output_channel = 6
output_id      = 0
osc            = 0
demod_id       = 0
sample_rate    = 200


# data readout from logger
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

if __name__ == '__main__':
    print('Connecting to lockin.')
    lockin = zi.ziLockin(device_id)
    
    print('Connecting to RF source.')
    rm = visa.ResourceManager()
    rf = rfsource.BNC865(rm)
    rf.power(16)
    rf.output(True)
    
    print('Configuring input.')
    lockin.configure_input(input_channel, input_range = 0.1,
                           ac_coupling=True, imp50 = False,
                           differential=False)
    demods = [0]
    harms = [1]
    
    print('Configuring demodulators.')
    for d, h in zip(demods,harms):
        lockin.configure_demodulator(d, sample_rate, input_channel = input_channel, filter_order=4, 
                                     tc = time_constant, osc=osc, harm=h)
    not_done = True
    try:
        while not_done:
            for r in range(reps):
                NA = len(As)
                kA = 0
                while kA < NA:
                    for rffreq in RFfreqs:
                        rf.frequency(rffreq)
                        A = As[kA]
                        T3 = lt.wait_for_T3(T3max)
                        # T3 = get_T3(lt)
                        
                        print('Starting ', A, 'V')
                        now = datetime.now()
                        timestamp = now.strftime('%Y%m%d-%H_%M_%S')
                        sweep_data = lockin.freq_sweep(freq_i, freq_f, samples, A, osc, output_id, output_channel, demods=demods,
                                                       settling_time=10*time_constant, settling_inaccuracy=10e-3,
                                                       avg_samples=navg, verbose=True, timeout=np.inf,
                                                       scan = zi.scan_sequential)
                        sweep_data['_drive_amplitude'] = A
                        sweep_data['_RF_freq'] = rffreq
                        sweep_data['_Tstart'] = T3
                        T3end = lt.get_T3()
                        sweep_data['_Tend'] = T3end
                        filename = 'FS-'+timestamp
                        np.save(path.join(output_dir, filename), sweep_data)
                        # if sweep_data['_Tend'] < T3max+0.3:
                            # kA += 1
                    kA += 1
            if not continuous:
                not_done = False
    finally:
        lockin.disable_everything()
        rf.output(False)
        rf.close()
        rm.close()