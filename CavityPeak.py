"""Fitting of microwave resonances. Ported from earlier matlab code."""

import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as intp
import scipy.optimize as optim
import fano_peak as fano


def pretty_print_parameters(params):
    """Print out the parameters formated in a readable way."""
    G, tau, phi0, phiZ, Qtot, Qext, w0 = params
    f0 = w0/2/np.pi
    Qint = 1/(1/Qtot - 1/Qext)

    print("f0 = {} GHz".format(f0/1e9))
    print("Qtot = {:.3f}, Qext = {:.3f}, Qint = {:.3f}".format(Qtot, Qext, Qint))
    print("phase offset = {:.3f} rad, impedance mismatch = {:.3f} rad".format(phi0, phiZ))
    print("G = {}".format(G))


def plot_circle(ax, cx, cy, R, N=512, fmt='-'):
    """Plot a cirlce in axes ax."""
    phis = np.linspace(0, 2*np.pi, N)
    xs = cx + R*np.cos(phis)
    ys = cy + R*np.sin(phis)
    ax.plot(xs, ys, fmt)


def fit_circle(xin, yin, debug=True):
    """
    Fit a circle to data given by xin, yin.

    This function is meant for datasets coming from resonances. These datasets put too much weight
    on the [0,0] region (many points in the tails). This function tries to get around that by
    resampling the data to equal spacing in phase using two-stage fitting.

    Parameters
    ----------
    **xin, yin** : data points, 1d real arrays
    """
    # scale the data
    scale = np.std(yin)
    x = xin/scale
    y = yin/scale

    if debug:
        fig, ax = plt.subplots(1, 1)
        ax.set_title("Circle fitting")
        ax.plot(x, y, "o")

    # initial fit to roughly estimate the centre and radius
    c0x = x.mean()
    c0y = y.mean()
    R = np.sqrt(x.std()**2 + y.std()**2)

    fun = lambda v: np.sum((((x - v[0])**2 + (y - v[1])**2 - v[2]**2))**2)
    p = optim.minimize(fun, x0=[c0x, c0y, R], tol=1e-8)

    # resample the dataset with equal spacing in phase
    c0x, c0y, R = p.x
    if debug:
        plot_circle(ax, c0x, c0y, R, fmt='--')
    xs = x - c0x
    ys = y - c0y
    rs = np.sqrt(xs**2 + ys**2)
    phis = np.unwrap(np.arctan2(ys, xs))
    ix = np.argsort(phis)
    phi = np.linspace(phis.min(), phis.max(), len(x))
    r = intp.interp1d(phis[ix], rs[ix])(phi)
    xnew = r*np.cos(phi)
    ynew = r*np.sin(phi)

    # fit again the resampled data
    fun = lambda v: np.sum((((xnew - v[0])**2 + (ynew - v[1])**2 - v[2]**2))**2)
    p = optim.minimize(fun, x0=[0, 0, R], tol=1e-8)
    cx, cy, RR = p.x
    if debug:
        plot_circle(ax, c0x + cx, c0y + cy, RR)
        fig, ax = plt.subplots(1, 1)
        ax.plot(xnew, ynew, 'o')
        plot_circle(ax, cx, cy, RR)

    out = [c0x + cx, c0y + cy, RR]

    return np.array(out)*scale


class CavityPeak():
    """Fit the microwave peak."""

    def __init__(self, f=None, X=None, Y=None, data_type='S11', normalization_order=0,
                 debug_plots=False,
                 prefit_peak=False):
        """
        Fit the microwave peak.

        This is a port of the matlab peak fitting code to python. Data can be loaded either
        from files or by supplying frequency and X and Y data directly.

        To load files do

        >>> peak = CavityPeak('/path/to/file.[txt|csv|npy]')

        Only the first three columns are read from the files and assigned to f, x, y. For text
        files, lines in the beggining which should be ignored must begin with '#'. Reading of csv
        files ignores first three lines. The numpy binary .npy should just contain an array with
        at least three columns.

        After loading the data, fit the peak using

        >>> peak.fit(background='auto|manual')

        With background set to 'manual' the program will ask for background ranges as in the matlab
        code. With 'auto' it will try to find a reasonable background range from a simple estimate
        of the peak width and position.

        After the fit, the Qs are printed in the outout and are stored in

        >>> peak.Qtot, peak.Qext, peak.Qint


        Parameters
        ----------
        f : array of fload or string, optional
            frequency or filename. The default is None.
        X : array of float, optional
            X-data. The default is None.
        Y : array of float, optional
            Y-data. The default is None.
        type : string, optional
            What kind of data to fit. Options are S11 and S21notch. Default is S11
        normalization_order : integer, optional
            Degree of the normalization polynomial. Default is 3.
        debug_plots : boolean, optional
            Whether to plot all the intermediate plots or just the final 3Done.
            The default is False.
        prefit_peak : boolean, optional
            Obtain the initial estimate of the peak with and position from a lorentzian-like
            fit to the x and y component. Wroks well for Fano-like peaks and greatly improves
            auto background, but works only when signal-to-noise is good enough.

        Raises
        ------
        RuntimeError
            Raises error if no data can be loaded.

        Returns
        -------
        None.

        """
        if None not in [f, X, Y]:
            self.f = f
            self.X = X
            self.Y = Y
        elif isinstance(f, str):
            print('loading file {}\n'.format(f))
            if '.CSV' in f.upper():
                data = np.loadtxt(f, delimiter=',', skiprows=3)
            elif '.NPY' in f.upper():
                data = np.load(f, allow_pickle=True).item()['data']
            else:
                data = np.loadtxt(f)
            self.f = data[:, 0]
            self.X = data[:, 1]
            self.Y = data[:, 2]
        else:
            raise RuntimeError('supply data in some way')

        self.omega = 2*np.pi*self.f
        self.debug_plots = debug_plots
        self.data_type = data_type
        self.normalization_order = normalization_order
        # this is called S11 but is not necessarily S11 for the S21 notch measurements
        self.S11 = self.X + 1j*self.Y
        self.R = np.abs(self.S11)
        self.phase = np.angle(self.S11)
        self.prefit = prefit_peak

        if self.prefit:
            ab = np.polyfit(self.f, self.Y, deg=1)
            p0 = [ab[1], ab[0], 0, 0, 0,
                  self.R.max() - self.R.min(),
                  self.f[np.argmin(self.R)],
                  1e6,
                  np.pi]
            peaky = fano.SingleFanoPeak(self.f, self.Y, plot=self.debug_plots, p0=p0)
            p0 = np.copy(peaky.popt)
            p0[-1] += np.pi/2
            peakx = fano.SingleFanoPeak(self.f, self.X, plot=self.debug_plots, p0=p0)

            self.peakx = peakx
            self.peaky = peaky
            print(peakx.popt)
            print(peaky.popt)

        if self.debug_plots:
            if self.prefit:
                self.peakx.ax.set_title('initial peak fit, X-component')
                self.peaky.ax.set_title('initial peak fit, Y-component')
                self.peakx.ax.legend(loc='best')
                self.peaky.ax.legend(loc='best')
            f, ax = plt.subplots(1, 1)
            f.suptitle('raw data')
            ax.plot(self.f, np.abs(self.S11))
            ax.set_xlabel('frequency')
            ax.set_ylabel('S11')

    def get_width_simple(self):
        """Estimate the peak position and width from just the R."""
        f0 = self.f[np.argmin(self.R)]
        Rmin = self.R.min()
        Rmax = np.percentile(self.R, 95)
        ix_half = self.R < (Rmin + 0.5*(Rmax - Rmin))
        fwhm = self.f[ix_half].max() - self.f[ix_half].min()
        return fwhm, f0

    def normalize_peak(self, baseline_ranges=None):
        """Normalize the peak by its baseline."""
        if self.prefit:
            self.fwhm = 0.5*(self.peaky.fwhm + self.peakx.fwhm)
            self.f0 = 0.5*(self.peaky.f0 + self.peakx.f0)
        else:
            self.fwhm, self.f0 = self.get_width_simple()
        self.f0ix = np.argmin(np.abs(self.f - self.f0))
        if baseline_ranges is None:
            # use the data far away from the central frequency
            # as the baseline
            ix_low = self.f < (self.f0 - 3*self.fwhm)
            ix_high = self.f > (self.f0 + 3*self.fwhm)
            fit_ix = np.logical_or(ix_low, ix_high)
        else:  # use user-supplied range
            fit_ix = np.zeros_like(self.f, dtype=bool)
            for rng in baseline_ranges:
                fl, fh = rng
                ix = np.logical_and(self.f > fl, self.f < fh)
                fit_ix = np.logical_or(fit_ix, ix)

        # store the indices of baseline fitting
        self.baseline_ix = fit_ix
        self.baseline_ix_low = np.logical_and(fit_ix, self.f < self.f0)
        self.baseline_ix_high = np.logical_and(fit_ix, self.f > self.f0)
        self.peak_ix = np.logical_not(self.baseline_ix)

        if self.debug_plots:
            fig, (axR, axN) = plt.subplots(1, 2, sharex=True)
            axR.plot(self.f, self.R)
            axR.plot(self.f[fit_ix], self.R[fit_ix], 'o', color='r')

        bg = np.polynomial.chebyshev.chebfit(self.f[fit_ix], self.R[fit_ix],
                                             deg=self.normalization_order)
        val_bg = np.polynomial.chebyshev.chebval(self.f, bg)

        self.S11Norm = self.S11 / val_bg

        if self.debug_plots:
            axR.plot(self.f, val_bg)
            axN.plot(self.f, np.abs(self.S11Norm))
            fig.suptitle('S11 normalized')
            axN.set_xlabel('frequency')
            axN.set_ylabel('S11, normalized')
            fig.tight_layout()

    def remove_delay(self):
        """Remove the cable delay from the signal."""
        phi = np.unwrap(np.angle(self.S11Norm))
        ix_low = self.baseline_ix_low
        ix_high = self.baseline_ix_high
        # ix_both = np.logical_or(ix_low, ix_high)
        if self.debug_plots:
            fig, (ax, axND) = plt.subplots(1, 2)
            ax.set_title('removing delay')
            ax.plot(self.f, phi)

        p_low = np.polyfit(self.f[ix_low], phi[ix_low], deg=1)
        p_high = np.polyfit(self.f[ix_high], phi[ix_high], deg=1)

        if self.debug_plots:
            ax.plot(self.f[ix_low], phi[ix_low], 'o', color='g')
            ax.plot(self.f, np.polyval(p_low, self.f), color='g')
            ax.plot(self.f[ix_high], phi[ix_high], 'o', color='r')
            ax.plot(self.f, np.polyval(p_high, self.f), color='r')

            delta = (np.polyval(p_high, self.f) - np.polyval(p_low, self.f)).mean()

            ax.plot(self.f, np.polyval(p_low, self.f) + delta, '--', color='g')
            ax.plot(self.f, np.polyval(p_high, self.f) - delta, '--', color='r')

        delay = (p_low[0] + p_high[0])/4/np.pi
        self.S11Norm_NoDelay = self.S11Norm*np.exp(-1j*self.omega*delay)
        self.delay = delay

        if self.debug_plots:
            axND.plot(self.f, np.unwrap(np.angle(self.S11Norm_NoDelay)))
            fig.tight_layout()

        return delay

    def shift_resonance(self):
        """Shift the resonance in the complex plane."""
        x = np.real(self.S11Norm_NoDelay)
        y = np.imag(self.S11Norm_NoDelay)

        if self.debug_plots:
            fig, ax = plt.subplots(1, 1)
            ax.plot(x, y, '+', ms=3)
            ax.set_aspect('equal')

        self.cx, self.cy, self.cR = fit_circle(x[self.peak_ix], y[self.peak_ix])
        if self.debug_plots:
            plot_circle(ax, self.cx, self.cy, self.cR)

        phase = np.unwrap(np.angle(self.S11Norm_NoDelay))
        if self.debug_plots:
            ff, aa = plt.subplots(1, 1)
            aa.set_title('phase offset')
            aa.plot(self.f, phase)
            aa.plot(self.f[self.baseline_ix], phase[self.baseline_ix], 'o')
        phase_l = np.mean(phase[self.baseline_ix_low])
        phase_h = np.mean(phase[self.baseline_ix_high])
        self.phase_offset = 0.5*(phase_l + phase_h)
        if phase_h < phase_l:  # wtf?
            self.phase_offset += np.pi
        print(phase_l, phase_h, self.phase_offset)
        self.S11Norm_NoDelay_Shifted = self.S11Norm_NoDelay - self.cx - 1j*self.cy
        self.S11Norm_NoDelay_Shifted *= np.exp(-1j*self.phase_offset)

        if self.debug_plots:
            fig, ax = plt.subplots(1, 1)
            x = np.real(self.S11Norm_NoDelay_Shifted)
            y = np.imag(self.S11Norm_NoDelay_Shifted)
            ax.plot(x, y, '+')
            ax.plot(x[self.baseline_ix], y[self.baseline_ix], 'o')

    def shift_phase(self):
        """Find estimate of total Q and plot shifted phase."""
        def theta_fit_fun(f, theta0, Qtot, A):
            return theta0 + 2*A*np.arctan(2*Qtot*(1-f/self.f0))

        phase = (np.angle(self.S11Norm_NoDelay_Shifted))
        v0 = [phase[self.f0ix], self.f0/self.fwhm, 1]
        if self.debug_plots:
            print("Initial fit paraemter estimates for phase:")
            print("\t", v0)

        v, vc = optim.curve_fit(theta_fit_fun, self.f[self.peak_ix], np.unwrap(phase[self.peak_ix]),
                                p0=v0, maxfev=5000)
        self.Qtot = v[1]
        self.phiZ0 = phase[self.baseline_ix].mean()

        if self.debug_plots:
            fig, ax = plt.subplots(1, 1)
            ax.set_title("Phase shifting Qtot={}.".format(self.Qtot))
            ax.plot(self.f, np.unwrap(phase))
            ax.plot(self.f[self.baseline_ix], np.unwrap(phase)[self.baseline_ix], 'o')
            ax.plot(self.f, theta_fit_fun(self.f, *v))
            ax.plot(self.f, theta_fit_fun(self.f, *v0), '--')
            fig.tight_layout()

    def fit_resonance(self):
        """Fits the full resonance peak."""
        def complex_peak_shape_S11(omega, A, tau, phi0, phiZ, Qtot, Qext, omega0):
            """Return the peak shape, including impedance mismatch."""
            peak = 1 / (1 + 2j*Qtot*(omega/omega0 - 1))
            phase_shift = np.exp(1j*omega*tau + 1j*phi0)
            Z_mismatch = np.exp(1j*phiZ)
            return A*phase_shift*(1 - 2*Z_mismatch*Qtot/Qext*peak)

        def complex_peak_shape_S21notch(omega, A, tau, phi0, phiZ, Qtot, Qext, omega0):
            """Return the peak shape, including impedance mismatch."""
            peak = 1 / (1 + 2j*Qtot*(omega/omega0 - 1))
            phase_shift = np.exp(1j*omega*tau + 1j*phi0)
            Z_mismatch = np.exp(1j*phiZ)
            return A*phase_shift*(1 - Z_mismatch*Qtot/Qext*peak)

        if self.data_type == 'S11':
            complex_peak_shape = complex_peak_shape_S11
        elif self.data_type == 'S21notch':
            complex_peak_shape = complex_peak_shape_S21notch
        else:
            raise RuntimeError("Unknown data type {}".format(self.data_type))

        def cost(v):
            peak = complex_peak_shape(2*np.pi*self.f, *v)
            return np.sum(np.abs(self.S11Norm - peak)**2)

        Qext = self.Qtot/self.cR
        v0 = [1, self.delay, self.phase_offset, self.phiZ0, self.Qtot, Qext, 2*np.pi*self.f0]
        print("Initial estimates of parameters:")
        pretty_print_parameters(v0)

        out = optim.minimize(cost, v0, method='Nelder-Mead', options={'maxiter': 10000})
        self.optimized_parameters = out.x
        print("Minimize finished: ", out.message)
        print("Optimized parameters:")
        pretty_print_parameters(self.optimized_parameters)

        fig, ax = plt.subplots(1, 1, subplot_kw={'projection': '3d'})
        ax.plot(self.f, np.real(self.S11Norm), np.imag(self.S11Norm), 'o')
        fit = complex_peak_shape(2*np.pi*self.f, *out.x)
        ax.plot(self.f, np.real(fit), np.imag(fit))
        fit0 = complex_peak_shape(2*np.pi*self.f, *v0)
        ax.plot(self.f, np.real(fit0), np.imag(fit0), '--')

        self.Qtot = out.x[4]
        self.Qext = out.x[5]
        self.Qint = 1/(1/self.Qtot - 1/self.Qext)
        ax.set_title('Qtot={:.3f}\n'
                     'Qext={:.3f}\n'
                     'Qint={:.3f}'.format(self.Qtot, self.Qext, self.Qint))

    def fit(self, debug_plots=False, background='auto'):
        """Fits the cavity peak."""
        if background == 'auto':
            baseline_ranges = None
        elif background == 'manual':
            fig, ax = plt.subplots(1, 1)
            ax.plot(self.f, self.R)
            ax.set_title('select 4 points for baseline\n'
                         '(left click = add point, right click = remove point')
            pts = fig.ginput(4)
            baseline_ranges = [(pts[0][0], pts[1][0]), (pts[2][0], pts[3][0])]
            plt.close(fig)
        else:
            print("Background options are only 'manual' or 'auto'.")
            return

        self.normalize_peak(baseline_ranges)
        self.remove_delay()
        self.shift_resonance()
        self.shift_phase()
        self.fit_resonance()


if __name__ == '__main__':
    # uncomment the following for the file dialog
    import tkinter
    import tkinter.filedialog
    root = tkinter.Tk()
    root.withdraw()
    file = tkinter.filedialog.askopenfilename()
    plt.close('all')
    # file = 'data/robyn.csv'
    print(file)
    peak = CavityPeak(file, data_type='S11',
                      debug_plots=True,  # with debug_plots=False it only shows the final 3D
                      normalization_order=4,
                      prefit_peak=False)
    peak.fit(background='manual')  # use background='manual' for the old-style background selection
