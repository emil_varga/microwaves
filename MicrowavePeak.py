# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 14:08:24 2022

@author: ev
"""

import numpy as np
import lmfit as lm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.signal import savgol_filter

def microwave_peak(f, A, A1, tau, phi0, phiZ, Qtot, Qext, f0):
    """Return the peak shape, including impedance mismatch."""
    peak = 1 / (1 + 2j*Qtot*(f/f0 - 1))
    phase_shift = np.exp(1j*f*tau + 1j*phi0)
    Z_mismatch = np.exp(1j*phiZ)
    return (A+A1*f)*phase_shift*(1 - 2*Z_mismatch*Qtot/Qext*peak)

class MicrowavePeak:
    def __init__(self, f, x, y, mask=[]):
        self.f = f/1e9
        self.x = x
        self.y = y
        
        ix = np.ones_like(self.f, dtype=bool)
        for (fl, fh) in mask:
            ix0 = np.logical_and(f > fl, f < fh)
            ix[ix0] = False
        self.f = self.f[ix]
        self.x = self.x[ix]
        self.y = self.y[ix]
        self.z = self.x + 1j*self.y
        self.model = lm.Model(microwave_peak, independent_vars='f')
        self.p0 = self.model.make_params(A=0, A1=0, tau=0, phi0=0, phiZ=0, Qtot=1e3, Qext=1e3, f0=10)
        
        #guess the initial parameters; the S11 peak is a dip, so the central frequency is near the minimum
        ix0 = abs(self.z) < np.percentile(abs(self.z), 10)
        ix1 = abs(self.z) > np.percentile(abs(self.z), 90)
        f0_guess = np.mean(self.f[ix0])
        m = np.mean(abs(self.z[ix0]))
        A_guess = np.mean(abs(self.z[ix1]))
        self.p0['f0'].set(value=f0_guess)
        self.p0['A'].set(value=A_guess)
        FWHM = 2*np.std(self.f[ix0])
        Qtot_guess = f0_guess/FWHM
        Qext_guess = Qtot_guess/(1 - m/A_guess)
        self.p0['Qtot'].set(value=Qtot_guess)
        self.p0['Qext'].set(value=Qext_guess)
        
        phi = np.angle(self.x + 1j*self.y)
        p = np.polyfit(self.f, phi, deg=1)
        self.p0['tau'].set(value=p[0])
        self.p0['phi0'].set(value=p[1])
        
    
    def fit(self, plot=False, smooth=True):
        self.out= self.model.fit(self.z, f=self.f, params=self.p0)
        if plot:
            print(self.out.fit_report())
            fig, ax = plt.subplots(2, 2, sharex=True)
            if smooth:
                ax[0,0].plot(self.f, self.x, 'o', ms=3, color='0.5')
                ax[0,1].plot(self.f, self.y, 's', ms=3, color='0.5')
                ax[0,0].plot(self.f, savgol_filter(self.x, 51, 3), 'o', ms=3)
                ax[0,1].plot(self.f, savgol_filter(self.y, 51, 3), 's', ms=3)
            else:
                ax[0,0].plot(self.f, self.x, 'o', ms=3)
                ax[0,1].plot(self.f, self.y, 's', ms=3)
            
            ax[0,0].plot(self.f, np.real(self.out.init_fit), ':')
            ax[0,1].plot(self.f, np.imag(self.out.init_fit), ':')
            
            ax[0,0].plot(self.f, np.real(self.out.best_fit), '--')
            ax[0,1].plot(self.f, np.imag(self.out.best_fit), '--')
            
            ax[1,0].plot(self.f, np.abs(self.x + 1j*self.y), 'o', ms=3)
            ax[1,0].plot(self.f, np.abs(self.out.init_fit), ':')
            ax[1,0].plot(self.f, np.abs(self.out.best_fit), '--')
            
            ax[1,1].plot(self.f, np.angle(self.x + 1j*self.y), 'o', ms=3)
            ax[1,1].plot(self.f, np.angle(self.out.init_fit), ':')
            ax[1,1].plot(self.f, np.angle(self.out.best_fit), '--')
            
            ax[1,0].set_xlabel('frequency')
            ax[1,1].set_xlabel('frequency')
            ax[0,0].set_ylabel('x')
            ax[0,1].set_ylabel('y')
            ax[1,0].set_ylabel('r')
            ax[1,1].set_ylabel('phi')
            fig.tight_layout()
            return fig, ax
    
    def plot3d(self):
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(self.f, self.x, self.y, 'o', ms=3)
        ax.plot(self.f, np.real(self.out.best_fit), np.imag(self.out.best_fit))

if __name__ == '__main__':
    import os.path as path
    from glob import glob
    
    rfdirs = glob('D:/ev/data/microwaves/RUN20220720/filled_full/600mK/spring_noise/pm_lock/5/*')
    rfdirs.sort()
    
    files = glob(path.join(rfdirs[0], '*.npy'))
    
    d = np.load(files[0], allow_pickle=True).item()
    
    f = d['vna_sweep'][:,0]
    x = d['vna_sweep'][:,1]
    y = d['vna_sweep'][:,2]
    
    mask_span = 100e3
    mask_center = d['RFfreq']
    
    masks = [(mask_center - mask_span/2 - shift, mask_center + mask_span/2 -shift) for shift in [0, 5e6]]
    
    r = np.abs(x + 1j*y)
    plt.close('all')
    peak = MicrowavePeak(f, x/r.mean(), y/r.mean(), mask=masks)
    fig, axs = peak.fit(plot=True)
    for ax in axs.flatten():
        ax.axvline(peak.out.best_values['f0'], ls='--')
    
    peak.plot3d()
        