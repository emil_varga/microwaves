#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:01:02 2019

@author: emil
"""

import numpy as np
import visa
import ziLockin as zi
from datetime import datetime
import os.path as path
import os
import ltwsclient as ltwsc
import instruments.rfsource as rfsource

device_id = 'dev538'
reps = 5
continuous = True

output_dir = 'data_RUN20200905/mechanics/RFvariable/SPRINGMECH58kHz'
f0 = 57.42e3
DAQrate = 3000 # Hz
DAQsamples = 8196 # this should give frequency resolutino of at least 0.25 Hz
time_constant = 1.5e-4
navg = 20
RFfreqs = np.linspace(2.7795e9, 2.7802e9, 50)
# RFfreqs = [2.7802e9]
T3max = 0.55

os.makedirs(output_dir,exist_ok=True)

# data readout from logger
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

if __name__ == '__main__':
    print('Connecting to lockin.')
    lockin = zi.ziLockin(device_id)
    
    print('Connecting to RF source.')
    rm = visa.ResourceManager()
    rf = rfsource.BNC865(rm)
    rf.power(16)
    rf.output(True)
    
    not_done = True
    try:
        while not_done:
            for rffreq in RFfreqs:
                print(f'Starting {rffreq/1e9} GHz')
                rf.frequency(rffreq)
                n = 0
                data_dir = path.join(output_dir, 'RF{:.6f}'.format(rffreq/1e9))
                os.makedirs(data_dir, exist_ok=True)
                while n < navg:
                    print(f'Sample {n}/{navg}, {rffreq/1e9} GHz')
                    try:
                        T3 = lt.wait_for_T3(T3max)
                    except RuntimeError:
                        print('Couldnt get start temperature.')
                        continue
                    
                    now = datetime.now()
                    timestamp = now.strftime('%Y%m%d-%H_%M_%S')
                    data = lockin.daq_continuous(f0, time_constant, DAQrate, DAQsamples)
                    data['_RF_freq'] = rffreq
                    data['_Tstart'] = T3
                    try:
                        T3end = lt.get_T3()
                    except RuntimeError:
                        print('Couldnt get end temperature.')
                        continue
                    data['_Tend'] = T3end
                    filename = 'FS-'+timestamp
                    np.save(path.join(data_dir, filename), data)
                    if data['_Tend'] < T3max+0.3:
                        n += 1
            if not continuous:
                not_done = False
    finally:
        lockin.disable_everything()
        rf.output(False)
        rf.close()
        rm.close()