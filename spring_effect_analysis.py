# -*- coding: utf-8 -*-
"""
Created on Tue Sep  8 16:24:20 2020

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from scipy.ndimage import gaussian_filter

import os.path as path
from glob import glob

# plt.close('all')

T = 0.9
dirs = glob('../data/microwaves/RUN20201214/filled/spring/T{:.0f}/0/RF*'.format(T*1000))
dirs.sort()
# dirs = glob('../data/microwaves/RUN20201214/spring/T750/RF*')
# T = 0.750
Ttol = 0.005
Nscale=1

fig3d = plt.figure()
ax3d = fig3d.add_subplot(111, projection='3d')

RFfreqs = []
img = []
len(dirs)
# fig, ax = plt.subplots(1, 1)
for dd in dirs:
    files = glob(path.join(dd, '*.npy'))
    print(dd, len(files))
    fftmean = 0
    freqmean = 0
    ampmean = 0
    k = 0
    # fig, ax = plt.subplots(1,1)

    for file in files:
        data = np.load(file, allow_pickle=True).item()
        if abs(data['Tstart'] - T) > Ttol or abs(data['Tend'] - T) > Ttol:
            print("Temperature out of range, skipping.")
            print(data['Tstart'], data['Tend'])
            continue
        # if abs(data['RF_freq'] - data['RF_cent']) > 0.5e6:
        #     continue
        
        # print(path.split(file)[-1], data['_RF_freq'])
    
        s = data['timeseries']
        if np.sum(np.isnan(s)) > 0:
            print("Nans in ", file)
            continue
        fft = np.fft.rfft(s - s.mean(), n=len(s)*Nscale)
        fftmean += np.abs(fft)
        freqmean += data['RF_freq']
        ampmean += np.max(np.abs(fft))
        k+=1
    if k == 0:
        continue
    
    f0 = freqmean/k
    RFfreqs.append(f0)
    # ax.set_title("{} GHz".format(freqmean/k/1e9))
    mechf = np.fft.rfftfreq(Nscale*data['DAQsamples'], 1/data['DAQrate'])
    fftmean = np.abs(fftmean)/k
    ax3d.plot(mechf, f0*np.ones_like(mechf), fftmean)
    clean = savgol_filter(fftmean, 21, 3)
    # ax.plot(mechf, fftmean)
    img.append(fftmean)

img = np.array(img)
tm = np.sum(img, axis=0)/img.shape[0]
# fig, ax = plt.subplots(1, 1)
# ax.plot(mechf, fftmean)
# bg = savgol_filter(tm, 51, 3)
# ax.plot(mechf, bg)

RFcent = data['RF_cent']
print(RFcent)
fig, ax = plt.subplots(1,1)
ix = np.logical_and(mechf > 100, mechf < 100000)
img = img[:,ix]
# img = img/(np.mean(img, axis=1)[:,np.newaxis])
ax.imshow(gaussian_filter(np.log(img), sigma=(0, 5)), aspect='auto', interpolation='nearest', origin='lower',
          extent=(mechf[ix].min(), mechf[ix].max(), np.min(RFfreqs)-RFcent, np.max(RFfreqs)-RFcent),
          cmap='inferno')#, vmin=-0.5, vmax=0.5)
# fig.savefig("spring.png", dpi=1200)

fig, ax = plt.subplots(1, 1)
ax.plot(mechf[ix], gaussian_filter(np.sum(img, axis=0), 5), label="{:.0f} mK".format(1000*T))
ax.legend(loc='best')
detuning = np.array(RFfreqs) - RFcent
dix = abs(detuning) < 1e6
np.savetxt('resp_T{:.0f}.txt'.format(100*T), np.column_stack((mechf[ix], np.median(img[dix,:], axis=0))))