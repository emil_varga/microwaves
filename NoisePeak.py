# -*- coding: utf-8 -*-
"""
Created on Wed Aug  3 08:07:47 2022

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig

import lmfit as lm

def noise_peak(f, f0, Gamma, A, b, b2):
    return b + b2*f + A*np.abs(Gamma*f0/(f0**2 - f**2 + 1j*Gamma*f))**2

def log_peak(f, f0, Gamma, A, b, b2):
    return np.log(noise_peak(f, f0, Gamma, A, b, b2))

class NoisePeak:
    def __init__(self, filename=None, freq=None, Sxx=None, frange=None, resolution=0.01, log=False):
        self.log = log
        if filename is not None:
            self.filename = filename
            d = np.load(self.filename, allow_pickle=True).item()
            self.rate = d['rate']
            self.N = d['samples']
            self.x = d['data'][0,:]
            self.y = d['data'][1,:]
            self.demod_freq = d['LIAfreq']
            freq, Sxx = sig.welch(self.x + 1j*self.y, fs=self.rate, nperseg=int(self.rate/resolution),
                                  return_onesided=False)
            freq += self.demod_freq
            freq = np.fft.fftshift(freq)
            Sxx = np.fft.fftshift(Sxx)
        else:
            self.freq = freq
            self.Sxx = Sxx
        
        if frange is not None:
            fi, ff = frange
            ix = np.logical_and(freq > fi, freq < ff)
            self.freq = freq[ix]
            self.Sxx = Sxx[ix]
            print(len(self.freq))
        
        if self.log:
            self.model = lm.Model(log_peak, independent_vars=['f'])
        else:
            self.model = lm.Model(noise_peak, independent_vars=['f'])
        self.init_model_parameters()
    
    def init_model_parameters(self):
        b = np.percentile(self.Sxx, 20)
        ix0 = np.argmax(self.Sxx)
        f0 = self.freq[ix0]
        A = self.Sxx[ix0] - b
        ixb = self.Sxx - b > 0.5*A
        Gamma = max(self.freq[ixb].max() - self.freq[ixb].min(), 0.05)
        self.init_params = self.model.make_params(f0=f0, Gamma=Gamma, A=A, b=b, b2=0)
    
    def use_p0(self, p0):
        for key in p0:
            self.init_params[key].set(value=p0[key])
    
    def fit(self):
        if self.log:
            self.result = self.model.fit(np.log(self.Sxx), f=self.freq, params=self.init_params, nan_policy='propagate')
        else:
            self.result = self.model.fit(self.Sxx, f=self.freq, params=self.init_params)
        
    def init_fit(self):
        y = self.model.eval(self.init_params, f=self.freq)
        if self.log:
            return np.exp(y)
        else:
            return y
    
    def best_fit(self):
        if self.log:
            return np.exp(self.result.best_fit)
        else:
            return self.result.best_fit

if __name__ == '__main__':
    from glob import glob
    import os.path as path
    plt.close('all')
    dirname = r'D:/ev/data/microwaves/RUN20220720/filled_full/600mK/spring_noise/pm_lock/4/RF_2306747586/'
    files = glob(path.join(dirname, '*.npy'))
    filename = files[0]
    peak = NoisePeak(filename, frange=(1377, 1381), log=True)
    fig, ax = plt.subplots(1, 1)
    ax.plot(peak.freq, peak.Sxx, 'o')
    ax.plot(peak.freq, peak.init_fit())
    
    peak.fit()
    ax.semilogy(peak.freq, peak.best_fit())
    
    peak2 = NoisePeak(filename, frange=(1377, 1381), log=False)
    peak2.init_params['b'].set(peak.result.best_values['b'], vary=False)
    # peak2.init_params['f0'].set(peak.result.best_values['f0'], vary=False)
    peak2.init_params['Gamma'].set(peak.result.best_values['Gamma'], vary=False)
    peak2.fit()
    print(peak.result.fit_report())
    # peak.result.plot()
    
    ax.semilogy(peak.freq, peak2.best_fit(), '--')