# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 17:58:31 2022

@author: emil
"""

import matplotlib as mp
mp.rcParams['font.size'] = 12
mp.rcParams['axes.spines.top'] = False
mp.rcParams['axes.spines.right'] = False

import numpy as np
import os.path as path
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

from glob import glob

def detrend(y):
    x = np.arange(len(y))
    p = np.polyfit(x, y, deg=1)
    return y# - np.polyval(p, x)

def rotate(I, Q):
    z = np.abs(I) + 1j*np.abs(Q)
    phi = np.angle(z)
    
    zz = I + 1j*Q
    return zz*np.cos(phi), 1j*zz*np.sin(phi)

def dbm2V(dbm):
    Z0 = 50
    W = 10**(dbm/10)/1000
    V = np.sqrt(Z0*W)
    return V

# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/buzzer/6'
# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/OMIT/am_lock/_3dBm_2'
base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/OMIT/am_lock_old/6'

rfdirs = glob(path.join(base_dir, 'RF*'))
rfdirs.sort()

plt.close('all')
figrf, axrf = plt.subplots(1, 1)
# figp, axp = plt.subplots(1, 1)

offset = 0

rffreqs = []

fig_avgs, axs_avg = plt.subplots(len(rfdirs), 1, sharex=True, sharey=True)
daxs_avg = {rd:ax for rd, ax in zip(rfdirs, np.atleast_1d(axs_avg))}

fall, axall = plt.subplots(1, 1)


for k, rfdir in enumerate(rfdirs[:]):
    avg_peak = 0
    avg_x = 0
    avg_y = 0
    fig, ax = plt.subplots(1, 1)
    ax.set_title(path.split(rfdir)[-1])
    files = glob(path.join(rfdir, '*.npy'))
    if len(files) == 0:
        continue
    print(rfdir)
    for file in files:
        d = np.load(file, allow_pickle=True).item()
        
        if d['cavity'] is not None:
            axrf.plot(d['cavity'][:,0], np.abs(d['cavity'][:,1] + 1j*d['cavity'][:,2]))    
        rffreqs.append(float(path.split(rfdir)[-1][3:]))
        
        freq = d['freq']
        I = d['I']
                
        # ax.plot(freq, np.abs(I))
        avg_peak += np.abs(I)
        avg_x += I.real
        avg_y += I.imag
        ax.plot(freq, I.imag)
        
    fig_avg, ax_avg = plt.subplots(2, 1, sharex=True, sharey=True)
    ax_avg[1].set_xlabel('AM frequency (Hz)')
    ax_avg[1].set_ylabel('y-homodyne signal')
    ax_avg[0].set_ylabel('x-homodyne signal')
    # ax_avg.plot(freq, avg_peak/len(files))
    # ax_avg.plot(freq, np.sqrt(avg_x**2 + avg_y**2)/len(files))
    ax_avg[0].plot(freq, avg_x/len(files), '-o', ms=2)
    ax_avg[1].plot(freq, avg_y/len(files), '-o', ms=2)
    ax_avg[0].set_title('avg '+path.split(rfdir)[-1])
    fig_avg.tight_layout()
    avg_peak_xy = np.sqrt(avg_x**2 + avg_y**2)/len(files)
    daxs_avg[rfdir].plot(freq, avg_peak*1e6)
    # daxs_avg[rfdir].plot(freq, avg_peak*1e6)
    # daxs_avg[rfdir].set_title('avg '+path.split(rfdir)[-1])
    axall.plot(freq, avg_peak*1e6)
    
    fig_avg_mag, ax_avg_mag = plt.subplots(1, 1)
    ax_avg_mag.plot(freq, avg_y*1e6, '-o')
    ax_avg_mag.set_xlabel(r'$\Omega/2\pi$ (Hz)')
    ax_avg_mag.set_ylabel(r'OMIT/OMIA (a.u.)')
    fig_avg_mag.tight_layout()

fig_avgs.tight_layout()