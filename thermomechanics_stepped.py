# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 16:15:05 2020

@author: Davis
"""


import numpy as np
import pyvisa as visa
import time
import os

import sys
sys.path.append('D:/ev')
from instruments.DAQcard import DAQcard
from instruments.DS345 import DS345

sys.path.append('C:/code/labtools')
import cryoutil as cu

data_dir = '../data/microwaves/RUN20220707/empty_ampm_stepped2/thermomechanics/T0'

T3max = 0.5

# #DAQ settings
daq_channels = ['ai0', 'ai1']
rate = 500 #Hz
samples = int(rate*8) #4 s
averages = 1

os.makedirs(data_dir, exist_ok=True)

rm = visa.ResourceManager()
cc = cu.get_cryocon(rm)
gen = DS345(rm, "GPIB0::6::INSTR")
#setup DAQ
daq = DAQcard(daq_channels, rate, samples)

lia_bandwidth = 160 #Hz
stepping = 80 #Hz

start_f = 30e3 #Hz
stop_f = 120e3 #Hz

center_f = start_f + stepping/2
try:
    while center_f < stop_f:
        gen.frequency(center_f)
        time.sleep(1)
        gotN = 0
        while gotN < averages:
            print("Starting {} Hz, {}/{}:".format(center_f, gotN+1, averages))
            Ti = cu.get_T3(cc)
            if Ti > T3max:
                time.sleep(1)
                continue
            timestamp = time.strftime('%Y%m%d-%H%M%S')
            timeseries = daq.measure()
            try:
                Tf = cu.get_T3(cc)
            except:
                Tf = np.nan
            output = {'timeseries': timeseries, 'rate': rate, 'samples': samples, 'fdemod': center_f,
                      'Ti': Ti, 'Tf': Tf}
            filename = os.path.join(data_dir, 'TM_{}.npy'.format( timestamp))
            np.save(filename, output, allow_pickle=True)
            gotN += 1
        center_f += stepping
finally:
    daq.close()
