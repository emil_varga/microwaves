# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 17:58:31 2022

@author: emil
"""

import numpy as np
import os.path as path
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

from glob import glob

def detrend(y):
    x = np.arange(len(y))
    p = np.polyfit(x, y, deg=1)
    return y# - np.polyval(p, x)

def rotate(I, Q):
    z = np.abs(I) + 1j*np.abs(Q)
    phi = np.angle(z)
    
    zz = I + 1j*Q
    return zz*np.cos(phi), 1j*zz*np.sin(phi)

def dbm2V(dbm):
    Z0 = 50
    W = 10**(dbm/10)/1000
    V = np.sqrt(Z0*W)
    return V

base_dirs = ['D:/ev/data/microwaves/RUN20220720/filled_full/buzzer/6',
             'D:/ev/data/microwaves/RUN20220720/filled_full/buzzer/700mK/1']

plt.close('all')
figT, axT = plt.subplots(1, 1)
axT.set_xlabel('buzzer frequency (Hz)')
axT.set_ylabel('normalized response')

for base_dir, l in zip(base_dirs, [1, 0]):
    rfdirs = glob(path.join(base_dir, 'RF*'))
    rfdirs.sort()

    figrf, axrf = plt.subplots(1, 1)
    # figp, axp = plt.subplots(1, 1)
    
    offset = 0
    
    rffreqs = []
    
    fig_avgs, axs_avg = plt.subplots(len(rfdirs), 1, sharex=True, sharey=True)
    daxs_avg = {rd:ax for rd, ax in zip(rfdirs, np.atleast_1d(axs_avg))}
    
    
    for k, rfdir in enumerate([rfdirs[l]]):        
        avg_peak = 0
        avg_x = 0
        avg_y = 0
        fig, ax = plt.subplots(1, 1)
        ax.set_title(path.split(rfdir)[-1])
        files = glob(path.join(rfdir, '*.npy'))
        if len(files) == 0:
            continue
        print(rfdir)
        for file in files:
            d = np.load(file, allow_pickle=True).item()
            
            if d['cavity'] is not None:
                axrf.plot(d['cavity'][:,0], np.abs(d['cavity'][:,1] + 1j*d['cavity'][:,2]))    
            rffreqs.append(float(path.split(rfdir)[-1][3:]))
            
            freq = d['freq']
            I = d['I']
            Q = d['Q']
            
            # if max(np.abs(I)) > 3e-7:
            #     continue
            amp = 1
            if 'lo_phase' in d:
                print(d['lo_phase'])
            if 'buzzer_power' in d:
                print('buzzer power: ', d['buzzer_power'])
                amp = dbm2V(d['buzzer_power'])
            
            # r = np.abs(Q)
            # ax.plot(freq, detrend(r))# - savgol_filter(r, 1001, 3))
            # ax.plot(freq, np.abs(I))
            # ax.plot(freq, np.abs(Q))
            # ax.plot(freq, r - r.mean())
            # ax.plot(freq, detrend(np.unwrap(np.angle(Q))))
            # offset += 7e-11
                    
            # ax.plot(freq, Q.real)
            # ax.plot(freq, Q.imag)
            # ax.plot(freq, I.real)
            ax.plot(freq, I.imag)
            # ax.plot(freq, np.abs(I)/amp)
            if d['buzzer_power'] > -5:
                avg_peak += np.abs(I)
                avg_x += I.real/amp
                avg_y += I.imag/amp
            # ax.plot(freq, savgol_filter(np.abs(I), 11, 3))
            # phi = detrend(np.unwrap(np.angle(I)))
            # phi = np.unwrap(np.angle(I))
            # axp.plot(freq, phi)
            # axp.plot(freq, savgol_filter(phi, 51, 3))
        fig_avg, ax_avg = plt.subplots(2, 1, sharex=True, sharey=True)
        ax_avg[1].set_xlabel('buzzer frequency (Hz)')
        ax_avg[1].set_ylabel('y-homodyne signal')
        ax_avg[0].set_ylabel('x-homodyne signal')
        # ax_avg.plot(freq, avg_peak/len(files))
        # ax_avg.plot(freq, np.sqrt(avg_x**2 + avg_y**2)/len(files))
        ax_avg[0].plot(freq, avg_x/len(files), '-o', ms=2)
        ax_avg[1].plot(freq, avg_y/len(files), '-o', ms=2)
        ax_avg[0].set_title('avg '+path.split(rfdir)[-1])
        fig_avg.tight_layout()
        avg_peak_xy = np.sqrt(avg_x**2 + avg_y**2)/len(files)
        daxs_avg[rfdir].plot(freq, avg_peak*1e6)
        # daxs_avg[rfdir].plot(freq, avg_peak*1e6)
        daxs_avg[rfdir].set_title('avg '+path.split(rfdir)[-1])
        axT.plot(freq, avg_peak/avg_peak.max())
    
    fig_avgs.tight_layout()
figT.tight_layout()