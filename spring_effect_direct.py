#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:01:02 2019

@author: emil
"""

import numpy as np
import visa
from datetime import datetime
import os.path as path
import os
import ltwsclient as ltwsc
import time
from scipy.ndimage import gaussian_filter

import sys
sys.path.append('D://ev')
from instruments.rfsource import BNC865
from instruments.SG384 import SG384
from instruments.DAQcard import DAQcard
from instruments.vna import VNA

def find_f0(vna_sweep):
    f = vna_sweep[:,0]
    x = vna_sweep[:,1]
    y = vna_sweep[:,2]
    r = np.abs(x + 1j*y)
    rs = gaussian_filter(r, 10)
    return f[np.argmin(rs)]

output_dir = '../data/microwaves/RUN20201214/filled/spring/T900/0'

reps = 10
continuous = True

DAQrate = 16384 # Hz
DAQsamples = int(DAQrate*64)
navg = 3
# RFcent = 5.31575e9 #1.7 K
# RFcent = 5.315146510e9 #1.8 K
RFcent = 5.314000071e9 #1.9 K
RF_tolerance = 1e5
RFspan = 1e6
RFN = 31

T3t = 0.9
T3_tolerance = 5e-3

os.makedirs(output_dir,exist_ok=True)
RFfreqs = np.linspace(RFcent - RFspan/2, RFcent + RFspan/2, RFN)
# np.random.shuffle(RFfreqs)

# data readout from logger
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

if __name__ == '__main__':  
    print('Connecting to RF source.')
    rm = visa.ResourceManager()
    rf = BNC865(rm, 'BNC845')
    rf.power(-15)
    rf.output(True)
    lo = SG384(rm, 'GPIB0::3::INSTR')
    
    vna = VNA(rm, 'USB0::0x0957::0x1509::MY51200747::INSTR')
    daq = DAQcard(['ai0'], DAQrate, DAQsamples)

    not_done = True
    try:
        while not_done:
            for rffreq in RFfreqs:
                print(f'Starting {rffreq/1e9} GHz')
                lo.lock()
                rf.frequency(rffreq)
                lo.frequency(rffreq)
                lo.unlock()
                print("Waiting 30 s")
                time.sleep(30)
                n = 0
                data_dir = path.join(output_dir, 'RF{:.6f}'.format(rffreq/1e9))
                os.makedirs(data_dir, exist_ok=True)
                peak_good = False
                while n < navg:
                    while True:
                        try:
                            T3 = lt.get_T3()
                        except:
                            T3 = np.nan
                        if np.isnan(T3) or abs(T3t - T3) > T3_tolerance:
                            print("waiting for T3 = {} to reach {}".format(T3, T3t))
                            time.sleep(1)
                            peak_good = False
                            continue
                        else:
                            break

                    timestamp = time.strftime('%Y%m%d-%H_%M_%S')
                    while not peak_good:
                        print("Getting a microwave sweep.")
                        vna_sweep = vna.sweep_cs(RFcent, RFspan, num_points=3000, bw=300)
                        if abs(RFcent - find_f0(vna_sweep)) > RF_tolerance:
                            print("Peak too far from target ({:.3f}).".format((RFcent-find_f0(vna_sweep))/1e6))
                            time.sleep(1)
                            continue
                        print("Peak on target, starting measuring.")
                        peak_good = True
                        vna_timestamp = timestamp
                    print(f'Sample {n}/{navg}, {rffreq/1e9} GHz')
                    
                    timeseries = daq.measure()
                    data = {}
                    data['timeseries'] = timeseries
                    data['DAQrate'] = DAQrate
                    data['DAQsamples'] = DAQsamples
                    data['RF_freq'] = rffreq
                    data['RF_cent'] = RFcent
                    data['Tstart'] = T3
                    data['vna_sweep']=vna_sweep
                    data['vna_timestamp'] = vna_timestamp
                    try:
                        T3end = lt.get_T3()
                    except:
                        print('Couldnt get end temperature.')
                        T3end = np.nan
                    data['Tend'] = T3end
                    filename = 'ts-'+timestamp
                    np.save(path.join(data_dir, filename), data)
                    if abs(data['Tend'] - T3t) < T3_tolerance:
                        n += 1
                    else:
                        peak_good = False
            if not continuous:
                not_done = False
    finally:
        rf.close()
        lo.close()
        rm.close()
        daq.close()