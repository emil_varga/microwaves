# -*- coding: utf-8 -*-
"""
Created on Fri Jul 21

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
from glob import glob
from scipy.signal import savgol_filter

cmap = plt.get_cmap('viridis')

# plt.close('all')

def get_amplitude(Ffile):
    d = np.load(Ffile, allow_pickle=True)
    return d.item()['amplitude_rms']


# data_dir = 'data_RUN20200905/mechanics/RF2.78GHz/THERMOMECH134kHz'
# data_dir = 'data_RUN20200905/mechanics/RF2.78GHz/MECH58kHz'
# data_dir = '../data/microwaves/RUN20201214/RF5GHz/AM/nf/T900/4/'
files = []

# data_dir = '../data/microwaves/RUN20201214/RF5GHz/BUZZ/T1000/1/'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../data/microwaves/RUN20201214/RF5GHz/BUZZ/T800/1/'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../data/microwaves/RUN20201214/RF5GHz/BUZZ/T700/1/'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../data/microwaves/RUN20201214/RF5GHz/BUZZ/T900/1/'
# files += glob(path.join(data_dir, '*.npy'))

# data_dir = '../data/microwaves/RUN20201214/RF5GHz/BUZZ/filled0/T800/2'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../data/microwaves/RUN20201214/RF5GHz/BUZZ/filled0/T700/1'
# files += glob(path.join(data_dir, '*.npy'))

# data_dir = '../data/microwaves/RUN20201214/filled/driven/BUZZ/T900/1'
# files += glob(path.join(data_dir, '*.npy'))

# data_dir = '../data/microwaves/RUN20201214/filled/driven/BUZZ/T800'
# files += glob(path.join(data_dir, '*.npy'))

# data_dir = '../data/microwaves/RUN20201214/filled/driven/BUZZ/T700/1'
# files += glob(path.join(data_dir, '*.npy'))

data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T800/0'
files += glob(path.join(data_dir, '*.npy'))

# files.sort()
print(len(files))
# files.sort(key=get_amplitude)
Amax = get_amplitude(files[-1])
Tmax = 0.755

fig1, ax1 = plt.subplots(1,1)
ax1.set_title('1st harmonic')
fig2, ax2 = plt.subplots(1,1)
ax2.set_title('2nd harmonic')
fig3, ax3 = plt.subplots(1,1)
ax3.set_title('3rd harmonic')
axs = [ax1, ax2, ax3]
Imean = {}
xmean = {}
ymean = {}
Ntm = {}
RFs = []

figrf, axrf = plt.subplots(1, 1)

colors = {900: 'k', 1000: 'r', 800: 'g', 700: 'b'}
for k, file in enumerate(files):
    print(file)
    file = np.load(file, allow_pickle=True).item()
    vna_sweep = file['vna_sweep']
    f = vna_sweep[:,0]
    S12 = np.sqrt(vna_sweep[:,1]**2 + vna_sweep[:,2]**2)
    # S12 = vna_sweep[:,1]
    
    
    data = file['data']
    T = file['temperatures'][-1][1]
    
    Tmk = int(round(T*10))*100
    if Tmk in colors:
        color = colors[Tmk]
    else:
        color=None
        
    axrf.plot(f, S12, color=cmap(k/len(files)))
    axrf.axvline(file['RF_freq'])

    freq = data[:,0]
    ix = np.argsort(freq)
    x = data[ix,1]
    y = data[ix,2]
    freq = freq[ix]
    I = np.sqrt(x**2 + y**2)
    
    amp  = file['amplitude_rms']
    # if amp < 1:
    #     continue
    h = file['harmonic']

    bgp = np.polyfit(freq, I, 5)
    bg = np.polyval(bgp, freq)
    print(T, amp)
    axs[h-1].plot(freq, I, color=color)#, label=file, color=cmap(0.5 + (RFfreq - 2.78e9)/0.02e9))
# for ax in axs:
#     ax.legend(loc='best')
