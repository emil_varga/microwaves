# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 15:06:14 2022

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import lmfit as lm
import scipy.signal as sig

import os.path as path
from glob import glob

from NoisePeak import NoisePeak
from MicrowavePeak import MicrowavePeak

def filter_response(f, tau, n=2):
    s = (1/(1 + 1j*(2*np.pi)*f*tau))**n
    return abs(s)

def spring(f, fc, f0m, g, kappa):
    delta = f - fc
    return f0m + delta*g**2/(1 + (2*delta/kappa)**2)**2


plt.close('all')
base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/spring_noise/0dBm/am_lock/1'
# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/600mK/spring_noise/am_lock/1'
resolution = 0.01 #Hz

rfdirs = glob(path.join(base_dir, 'RF*'))
rfdirs.sort()

rffreqs = []
widths = []
f0s = []
As = []
fscavity = []

widths_f = {}
f0s_f = {}
As_f = {}
fcavity = {}
p0 = None
for rfdir in rfdirs:
    files = glob(path.join(rfdir, '*.npy'))
    Sxx_avg = 0
    print("{}: {}".format(rfdir, len(files)))
    if len(files) == 0:
        continue
    fig, ax = plt.subplots(1, 1)
    ax.set_title(path.split(rfdir)[-1])
    Sxx_mean = 0
    for file in files:
        d = np.load(file, allow_pickle=True).item()
        rate = d['rate']
        N = d['samples']
        x = d['data'][0,:]
        y = d['data'][1,:]
        demod_freq = d['LIAfreq']
        freq, Sxx = sig.welch(x + 1j*y, fs=rate, nperseg=int(rate/resolution),
                              return_onesided=False)
        freq += demod_freq
        freq = np.fft.fftshift(freq)
        Sxx = np.fft.fftshift(Sxx)
        Sxx_mean += Sxx
    Sxx_mean /= len(files)
    try:
        peak = NoisePeak(freq=freq, Sxx=Sxx_mean, frange=(1392, 1393.2), log=True)
        if p0 is not None:
            # peak.use_p0(p0)
            peak.use_p0({'f0': 1392.6, 'Gamma': 0.05, 'b': 1e-3, 'b2': 0, 'A':1e-2})
        peak.fit()
        print(peak.result.best_values)
        rff = d['vna_sweep'][:,0]
        rfx = d['vna_sweep'][:,1]
        rfy = d['vna_sweep'][:,2]
        fd = d['RFfreq']
        dw = 100e3
        mwpeak = MicrowavePeak(rff, rfx, rfy, mask=[(fd-dw, fd+dw),(fd-dw-5e6, fd+dw-5e6)])
        mwpeak.fit()
        pl = ax.plot(peak.freq, peak.Sxx, 'o', ms=3)
        ax.semilogy(peak.freq, peak.best_fit(), '-', color=pl[-1].get_color())
        p0 = peak.result.best_values
        RFfreq = d['RFfreq']
        rffreqs.append(RFfreq)
        widths.append(peak.result.best_values['Gamma'])
        f0s.append(peak.result.best_values['f0'])
        As.append(peak.result.best_values['A'])
        fscavity.append(mwpeak.out.best_values['f0'])
        if RFfreq in widths_f:
            widths_f[RFfreq].append(peak.result.best_values['Gamma'])
            f0s_f[RFfreq].append(peak.result.best_values['f0'])
            As_f[RFfreq].append(peak.result.best_values['A'])
        else:
            widths_f[RFfreq] = [peak.result.best_values['Gamma']]
            f0s_f[RFfreq] = [peak.result.best_values['f0']]
            As_f[RFfreq] = [peak.result.best_values['A']]
    except Exception as e:
        print("Fit failed!", e)
        print(peak.init_params)
        continue

rffreqs = np.array(rffreqs)
f0s = np.array(f0s)
As = np.array(As)
widths = np.array(widths)
fscavity = np.array(fscavity)

fig, ax = plt.subplots(1, 1)
ax.set_xlabel(r'$\Delta$ (Hz)')
ax.set_ylabel(r'$\Omega$')
ax.plot(rffreqs - fscavity*1e9, f0s, 'o')

rffs = []
f0sf = []
f0sfe = []
for rffreq in f0s_f:
    rffs.append(rffreq/1e9)
    f0sf.append(np.mean(f0s_f[rffreq]))
    f0sfe.append(np.std(f0s_f[rffreq]))
rffs = np.array(rffs)
f0sf = np.array(f0sf)
f0sfe = np.array(f0sfe)

fig, ax = plt.subplots(2, 1, sharex=True)
ax[0].plot(rffreqs/1e9, widths, 'o')
ax[1].plot(rffreqs/1e9, f0s, 'o')
ax[1].errorbar(rffs, f0sf, yerr=f0sfe, fmt='o', zorder=99)
ax[0].set_ylabel('peak height (a.u.)')
ax[1].set_ylabel(r'$\Omega_m/2\pi$ (Hz)')
ax[1].set_xlabel(r'$\omega/2\pi$ (GHz)')

model = lm.Model(spring)
pars = model.make_params(fc=2.30692, f0m=1379.5, g=1, kappa=1e-3)
init_fit = model.eval(params=pars, f=rffreqs/1e9)
pars['g'].set(10, min=0)
# ax[1].plot(rffreqs/1e9, init_fit)
fit = model.fit(f0s, f=rffreqs/1e9, params=pars)
ax[1].plot(rffreqs/1e9, fit.best_fit, '-')
print(fit.fit_report())
fig.tight_layout()