import websocket # pip install websocket_client
import json
import time
import numpy as np

class LTWebsockClient(object):
    def __init__(self, server, port):
        
        self.ws = websocket.create_connection(f"ws://{server}:{port}/socket")
        self.ws.settimeout(2) # 2 seconds
        
        # on connect, server sends info of all measurements saved on server
        r = self.wait_for("update_measurements")
        self.measurements = r['measurements']
    
    
    def call(self, action, **kwargs):
        kwargs.update(action=action)
        self.ws.send(json.dumps(kwargs))
        
    def recv(self):
        msg = self.ws.recv()
        return json.loads(msg)

    def wait_for(self, action):
        waiting = True
        while waiting:
            msg = self.recv()

            if msg['action'] == action:
                return msg

    @property
    def names(self):
        return list(self.measurements.keys())
        
    def get_last_measurements(self, name, dt):
        """get the recorded measurements with name over the last dt seconds
        
        Args:
            name (str): Name of measurement to get
            dt (float): Get all measurements recorded in last dt seconds
            
        Returns: 
            iterable of shape [N][2], conatining N pairs of (time, value) records
        """
        self.call("get_data", 
            name = name,
            ta = (time.time() - dt) * 1000) # javascript timestamps are in ms :(
        resp = self.wait_for("got_data")
        
        data = np.asarray(resp['data'])
        
        if len(data) > 0:
            # convert to second timestamps
            data = data * np.array([.001, 1])
            
        return data
    
    def wait_for_T3(self, cutoff_T3, sleep_time=5):
        print(f"Waiting for T3 to go below {cutoff_T3}.")
        attempts = 0
        while True:
            try:
                last_5min = self.get_last_measurements('3hepot_t', 15*60)
                if len(last_5min) == 0:
                    print("Failed to get logger data.")
                    continue
                attempts = 0
            except:
                attempts+=1
                if attempts > 5:
                    print('cant get data from logger')
                    raise
                continue
            #check that the data are up-to-date
            if abs(time.time() - last_5min[-1][0]) > 600:
                raise RuntimeError('old data from logger')
            #there is some weird offset of about 80 s between the logger time and local time
            t = last_5min[:, 0]
            t0 = t[-1]
            T3 = last_5min[t > t0-10,1].mean() #average only over the last 10 s
            print(f"T3 is {T3}")
            if T3 < cutoff_T3:
                break
            time.sleep(sleep_time)
        return T3

    def wait_for_T3_notry(self, cutoff_T3, sleep_time=5):
        print(f"Waiting for T3 to go below {cutoff_T3}.")
        while True:
            last_5min = self.get_last_measurements('3hepot_t', 5*60)
            #check that the data are up-to-date
            if abs(time.time() - last_5min[-1][0]) > 600:
                raise RuntimeError('old data from logger')
            #there is some weird offset of about 80 s between the logger time and local time
            t = last_5min[:, 0]
            t0 = t[-1]
            T3 = last_5min[t > t0-10,1].mean() #average only over the last 10 s
            print(f"T3 is {T3}")
            if T3 < cutoff_T3:
                break
            time.sleep(sleep_time)
        return T3

    def get_T3(self):
        last_5min = self.get_last_measurements('3hepot_t', 10*60)
        t = last_5min[:, 0]
        t0 = t[-1]
        T3 = last_5min[t > t0-10,1].mean() #average only over the last 10 s
        return T3


if __name__ == '__main__':
    server = "onnes.ccis.ualberta.ca"
    port = "3172"
    print(f"connecting to {server}:{port}")

    lt = LTWebsockClient(server, port)
    print("connected")
    print(f"all measurement names: {lt.names}")

    last_5min = lt.get_last_measurements(lt.names[0], 5*60)

    print(f"last measurement of {lt.names[0]} from last 5 minutes is")
    print(last_5min[:,1].mean())
    print(time.time() - last_5min[-1][0])