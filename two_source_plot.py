import numpy as np
import os.path as path
import matplotlib.pyplot as plt
from glob import glob

base_dir = r'D:\ev\data\microwaves\RUN20220720\filled_full\425mbar\600mK\OMIT\two_source\2'

dirs = glob(path.join(base_dir, 'pump*'))

plt.close('all')

for dd in dirs:
    files = glob(path.join(dd, 'OMIT2*'))
    
    fig, ax = plt.subplots(1, 1)
    ax.set_title(path.split(dd)[-1])
    
    avg = 0
    for file in files:
        data = np.load(file, allow_pickle=True).item() #what does the .item() do?
        z = data['data']    #x+iy
        fm = data['freq']   #mechanical freqs Hz
        mag = abs(z)    # V
        avg += mag
        ax.plot(fm, mag)
        
        # fig_vna, ax_vna = plt.subplots(1, 1)
        # rff = data['vna_sweep'][:,0]
        # rfx = data['vna_sweep'][:,1]
        # rfy = data['vna_sweep'][:,2]
        # ax_vna.plot(rff, np.abs(rfx + 1j*rfy))
    
    fig, ax = plt.subplots(1, 1)
    ax.set_title('average '+path.split(dd)[-1])
    ax.plot(fm, avg)