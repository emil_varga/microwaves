# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 16:15:05 2020

@author: Davis
"""


import numpy as np
import pyvisa as visa
import time
import os

import sys
sys.path.append('D:/ev')
from instruments.DAQcard import DAQcard

sys.path.append('C:/code/labtools')
import cryoutil as cu

data_dir = '../data/microwaves/RUN20220707/empty_direct_higham_offpeak/thermomechanics/T0'

T3max = 0.5

# #DAQ settings
daq_channels = ['ai0', 'ai1']
rate = 250000 #Hz
samples = int(rate*64) #64 s
averages = 5

os.makedirs(data_dir, exist_ok=True)

rm = visa.ResourceManager()
cc = cu.get_cryocon(rm)

#setup DAQ
daq = DAQcard(daq_channels, rate, samples)

def get_averaged_spectrum(daq, avg):
    freqs = np.fft.fftfreq(daq.samples, 1/daq.rate)
    freqs = np.fft.fftshift(freqs)
        
    avgresp = 0
    for k in range(avg):
        print(k)
        data1 = daq.measure()
        resp = np.fft.fft(data1[0, :] + 1j*data1[1, :])
        resp = np.fft.fftshift(resp)
        avgresp += np.abs(resp)
    avgresp = avgresp/avg
    return freqs, avgresp, data1

try:
    gotN = 0
    while gotN < averages:
        print("Starting {}/{}:".format(gotN+1, averages))
        Ti = cu.get_T3(cc)
        if Ti > T3max:
            time.sleep(1)
            continue
        timestamp = time.strftime('%Y%m%d-%H%M%S')
        timeseries = daq.measure()
        try:
            Tf = cu.get_T3(cc)
        except:
            Tf = np.nan
        output = {'timeseries': timeseries, 'rate': rate, 'samples': samples, 'fdemod': 0,
                  'Ti': Ti, 'Tf': Tf}
        filename = os.path.join(data_dir, 'TM_{}.npy'.format( timestamp))
        np.save(filename, output, allow_pickle=True)
        gotN += 1
finally:
    daq.close()
