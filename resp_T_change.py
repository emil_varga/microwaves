# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 12:05:48 2020

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt

T900 = np.loadtxt('resp_T90.txt')
T800 = np.loadtxt('resp_T80.txt')
T700 = np.loadtxt('resp_T70.txt')

fig, ax = plt.subplots(1, 1)
ax.plot(T700[:,0], T700[:,1]/T700[:,1].mean())
ax.plot(T800[:,0], T800[:,1]/T800[:,1].mean())
ax.plot(T900[:,0], T900[:,1]/T900[:,1].mean())