# -*- coding: utf-8 -*-
"""
Created on Fri Jul 21

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
from glob import glob
from scipy.ndimage import gaussian_filter

cmap = plt.get_cmap('viridis')

plt.close('all')


files = []

# data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T900/red/0'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T800/red/0'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T800/red/1'
# files += glob(path.join(data_dir, '*.npy'))

data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T800/blue/0'
files += glob(path.join(data_dir, '*.npy'))

print(len(files))

fig1, ax1 = plt.subplots(1,1)
fig2, ax2 = plt.subplots(1,1)
fig3, ax3 = plt.subplots(1,1)
figall, axall = plt.subplots(1, 1)
axs = [ax1, ax2, ax3]
Imean = {}
xmean = {}
ymean = {}
Ntm = {}
RFs = []

figrf, axrf = plt.subplots(1, 1)

colors = {900: 'k', 1000: 'r', 800: 'g', 700: 'b'}
avgT = 0
for k, file in enumerate(files):
    print(file)
    file = np.load(file, allow_pickle=True).item()
    vna_sweep = file['vna_sweep']
    f = vna_sweep[:,0]
    S12 = np.sqrt(vna_sweep[:,1]**2 + vna_sweep[:,2]**2)
    # S12 = vna_sweep[:,1]
    
    
    data = file['data']
    T = file['temperatures'][-1][1]
    avgT += T
    
    Tmk = int(round(T*10))*100
    if Tmk in colors:
        color = colors[Tmk]
    else:
        color=None
        
    axrf.plot(f, S12, color=cmap(k/len(files)))
    axrf.axvline(file['RF_freq'])

    freq = data[:,0]
    ix = np.argsort(freq)
    Ix = data[ix,1]
    Iy = data[ix,2]
    Qx = data[ix,3]
    Qy = data[ix,4]
    I = Ix + 1j*Iy
    Q = Qx + 1j*Qy
    r = np.abs(I + 1j*Q)
    freq = freq[ix]
    # r = np.sqrt((Ix - Qy)**2 + (Iy + Qx)**2)
    if 'drive_power_dbm' in file:
        amp = file['drive_power_dbm']
    else:
        amp = -10
    h = file['harmonic']

    print(T, amp)
    axs[h-1].plot(freq, r, '-o')#, color=cmap((amp+5)/5))#, label=file, color=cmap(0.5 + (RFfreq - 2.78e9)/0.02e9))
    bgp = np.polyfit(freq, r, deg=3)
    bg = np.polyval(bgp, freq)
    # bg = gaussian_filter(r, sigma=1)
    axall.plot(freq, (r-bg)/r.mean(), '-o', color=color)
# for ax in axs:
#     ax.legend(loc='best')
avgT = avgT/len(files)
ax1.set_title('1st harmonic, {:.0f} mK'.format(avgT*1000))
ax2.set_title('2nd harmonic, {:.0f} mK'.format(avgT*1000))
ax3.set_title('3rd harmonic, {:.0f} mK'.format(avgT*1000))
axall.set_title("all harmonics, {:.0f} mK".format(avgT*1000))