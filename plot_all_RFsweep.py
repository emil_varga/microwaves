# -*- coding: utf-8 -*-
"""
Created on Sun Aug  9 15:11:11 2020

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt

import os.path as path
from glob import glob

dirs = glob('../data/RUN_20200917/RFvariable/MECH85kHz/RF*')
dirs.sort()

cmap = plt.get_cmap('viridis')

plt.close('all')
fig, ax = plt.subplots(1,1)
avg_line = 0
N = 0
f0 = 2.7237e9
span = 10e6
def scale(f, f0=f0, span=span):
    if f < f0 - span/2:
        return 0
    elif f > f0 + span/2:
        return 1
    else:
        return (f - f0)/span + 0.5
img = []
RFfreqs = []
for dd in dirs:
    print(dd)
    files = glob(path.join(dd, '*.npy'))
    avg = 0
    RFavg = 0
    if len(files) == 0:
        continue
    for file in files:
        df = np.load(file, allow_pickle=True).item()
        if df['T3final'] > 0.5:
            continue
        d = df['data']
        f = d[:,0]
        # ix = np.logical_and(f > 87500, f < 92500)
        r = np.sqrt(d[:,1]**2 + d[:,2]**2)
        A = df['amplitude_rms']
        RF = df['RF_freq']
        if A == 0.2:
            avg_line += r
            N += 1
        ax.plot(f/1e3, r, label=path.split(file)[-1], color=cmap(scale(RF)))
        avg += r
        RFavg += RF
        # np.savetxt(file.replace('.npy', '.txt'), d)
    avg /= len(files)
    img.append(avg)
    RFfreqs.append(RFavg/len(files))

ax.set_xlabel('freq (kHz)')
# ax.legend(loc='best')
    

fig, ax = plt.subplots(1, 1)
ax.plot(f, avg_line)

fig, ax = plt.subplots(1,1)
ax.imshow(img, aspect='auto', origin='lower', extent=(f[0], f[-1], RFfreqs[0], RFfreqs[-1]))