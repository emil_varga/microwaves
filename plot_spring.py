# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 17:58:31 2022

@author: emil
"""

import numpy as np
import os.path as path
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

from glob import glob

import matplotlib as mpl # in python

C = np.loadtxt('colormap.txt')

def detrend(y):
    x = np.arange(len(y))
    p = np.polyfit(x, y, deg=1)
    return y# - np.polyval(p, x)

def rotate(I, Q):
    z = np.abs(I) + 1j*np.abs(Q)
    phi = np.angle(z)
    
    zz = I + 1j*Q
    return zz*np.cos(phi), 1j*zz*np.sin(phi)

def dbm2V(dbm):
    Z0 = 50
    W = 10**(dbm/10)/1000
    V = np.sqrt(Z0*W)
    return V

# cmap = plt.get_cmap('rgb')
cmap = mpl.colors.ListedColormap(C/255.0)

# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/buzzer/6'
base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/buzzer/700mK/spring_driven/am_lock/1'

rfdirs = glob(path.join(base_dir, 'RF*'))
rfdirs.sort()

plt.close('all')

offset = 0

rffreqs = []

figall, axall = plt.subplots(1, 1)

for k, rfdir in enumerate(rfdirs[:]):
    avg_peak = 0
    avg_x = 0
    avg_y = 0
    n = 0
    files = glob(path.join(rfdir, '*.npy'))
    if len(files) == 0:
        continue
    print(rfdir)
    RFfreq = float(path.split(rfdir)[-1][3:])
    rffreqs.append(RFfreq)
    for file in files:
        d = np.load(file, allow_pickle=True).item()
        
        
        freq = d['freq']
        I = d['I']
        Q = d['Q']
        
        # if max(np.abs(I)) > 3e-7:
        #     continue
        amp = 1
        if 'lo_phase' in d:
            print(d['lo_phase'])
        if 'buzzer_power' in d:
            print('buzzer power: ', d['buzzer_power'])
            amp = dbm2V(d['buzzer_power'])
        
        if d['buzzer_power'] > -5:
            avg_peak += np.abs(I)
            avg_x += I.real/amp
            avg_y += I.imag/amp
            n += 1
    
    avg_x /= n
    avg_y /= n
    avg_peak /=n
    color = (RFfreq - 2.3061e9)/4e6 + 0.5
    axall.plot(freq, avg_peak*1e6, color=cmap(color))

figall.tight_layout()