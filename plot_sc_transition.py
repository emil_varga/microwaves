# -*- coding: utf-8 -*-
"""
Created on Sun Aug  9 15:11:11 2020

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

from scipy.signal import savgol_filter

import os.path as path
from glob import glob

# mpl.rc('font', size=18)

# files = glob(path.join('../data/microwaves/RUN20201214/filled/cooldowns/T700', '*.npy'))
files = glob(path.join(r'D:\ev\data\microwaves\RUN20220720\filled_full\cooldowns\1', '*.npy'))
files.sort(key=lambda f: np.load(f, allow_pickle=True).item()['Tstart'])
# files = files[::5]
# files = files[::-1]
# files = files[::5]

cmap = plt.get_cmap('jet')
plt.close('all')
fig, ax = plt.subplots(1,1)#, figsize=(8,6))
Tmax = 1.3
Tmin = 0.6
for file in files[::-1]:
    dd = np.load(file, allow_pickle=True)
    Ti = dd.item()['Tstart']
    Tf = dd.item()['Tend']
    T = 0.5*(Ti + Tf)
    # if T > Tmax or T < Tmin:
    #     continue
    print(Ti, Tf)
    d = dd.item()['data']
    f = d[:,0]
    x = d[:,1]
    y = d[:,2]
    z = x + 1j*y
    r = np.sqrt(x**2 + y**2)
    r = savgol_filter(r, 51, 3)
    ax.plot(f/1e9, r, label=path.split(file)[-1], color=cmap((T-Tmin)/(Tmax-Tmin)))
    # ax.plot(f/1e9, np.unwrap(np.angle(z)), label=path.split(file)[-1], color=cmap((T-Tmin)/(Tmax-Tmin)))
    # ax.plot(f/1e9, x, label=path.split(file)[-1], color=cmap((T-Tmin)/(Tmax-Tmin)))
    # np.savetxt(file.replace('.npy', '.txt'), d)

ax.set_xlabel('frequency (GHz)')
ax.set_ylabel('cavity reflection (S11)')
# ax.legend(loc='best')
# ax.set_xlim(5.32, 5.38)

# cax = fig.add_axes([0.2, 0.3, 0.5, 0.05])
sm = plt.cm.ScalarMappable(mpl.colors.Normalize(vmin=Tmin, vmax=Tmax), cmap='jet')
# cbar = fig.colorbar(sm, cax=cax, orientation='horizontal')
cbar = fig.colorbar(sm)
cbar.set_label("Temperature (K)")
fig.tight_layout()
