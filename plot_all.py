# -*- coding: utf-8 -*-
"""
Created on Sun Aug  9 15:11:11 2020

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt

import os.path as path
from glob import glob

files = glob(path.join('../data/RUN_20200917/RF2.7235GHz/MECH85kHz/fine', '*.npy'))
files.sort()

cmap = plt.get_cmap('viridis')

plt.close('all')
fig, ax = plt.subplots(1,1)
avg_line = 0
N = 0
for file in files:
    df = np.load(file, allow_pickle=True).item()
    d = df['data']
    f = d[:,0]
    r = np.sqrt(d[:,1]**2 + d[:,2]**2)
    A = df['amplitude_rms']
    if A == 0.2:
        avg_line += r
        N += 1
    ax.plot(f/1e3, r, label=path.split(file)[-1], color=cmap(A/0.2))
    # np.savetxt(file.replace('.npy', '.txt'), d)

ax.set_xlabel('freq (kHz)')
# ax.legend(loc='best')
    

fig, ax = plt.subplots(1, 1)
ax.plot(f, avg_line/N)