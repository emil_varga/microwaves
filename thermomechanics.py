# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 16:15:05 2020

@author: Davis
"""


import numpy as np
import visa
import matplotlib.pyplot as plt
import time
import os
import ltwsclient

import sys
sys.path.append('D:\\ev')
from instruments.DAQcard import DAQcard
from instruments.SR830 import SR830
from instruments.SG384 import SG384
from instruments.rfsource import BNC865
from instruments.KS33210A import KS33210A #for noise drive
from instruments.vna import VNA

print('Connecting to logger.')
server = 'onnes.ccis.ualberta.ca'
port = 3172
lt = ltwsclient.LTWebsockClient(server, port)
print('Connected')

data_dir = '../data/microwaves/RUN20201214/thermomechanics/T750'

T3max = 0.755

#DAQ settings
daq_channels = ['ai6', 'ai7']
rate = 16192 #Hz
samples = int(rate*64) #32 s
averages = 10

#lockin settings
fdemod = 4000 #Hz
sens = '5m' # V
tc = '300u' # s
slope = '6' #dB/oct

# RF settings
# RFfreq = 5.36651e9 #Hz
RFfreq = 5365715421 #Hz
RFpower = -15 #dBm

#noise drive
use_noise_drive = True
noise_amplitude = [0, 0.05, 0.1, 0.5, 1, 5] # V

os.makedirs(data_dir, exist_ok=True)

rm = visa.ResourceManager()
rf = BNC865(rm, 'BNC845')
lo = SG384(rm, 'GPIB0::3::INSTR')
lockin = SR830(rm, 'GPIB0::1::INSTR')
gen = KS33210A(rm, '33210A')

rm_ag = visa.ResourceManager('C:\\Program Files (x86)\\IVI Foundation\\VISA\\WinNT\\agvisa\\agbin\\visa32.dll')
vna = VNA(rm_ag)

#setup RF
rf.frequency(RFfreq)
rf.power(RFpower)
rf.output(True)
lo.lock()
lo.frequency(RFfreq)
lo.unlock()

#setup lockin
lockin.lock()
lockin.coupling('ac')
lockin.set_reference('internal')
lockin.set_frequency(fdemod)
lockin.set_timeconstant(tc)
lockin.set_sensitivity(sens)
lockin.set_slope(slope)
lockin.harmonic(1)
lockin.unlock()

#setup DAQ
daq = DAQcard(daq_channels, rate, samples)

#setup noise drive
gen.function('NOISE')
gen.amplitude(noise_amplitude[0])
gen.output(use_noise_drive)
rf.am(use_noise_drive, 0.1)

def get_averaged_spectrum(daq, avg):
    freqs = np.fft.fftfreq(daq.samples, 1/daq.rate)
    freqs = np.fft.fftshift(freqs)
        
    avgresp = 0
    for k in range(avg):
        print(k)
        data1 = daq.measure()
        resp = np.fft.fft(data1[0, :] + 1j*data1[1, :])
        resp = np.fft.fftshift(resp)
        avgresp += np.abs(resp)
    avgresp = avgresp/avg
    return freqs, avgresp, data1

try:
    while True:
        for noiseA in noise_amplitude:
            lt.wait_for_T3(T3max)
            timestamp = time.strftime('%Y%m%d-%H%M%S')
            gen.amplitude(0)
            print("Measuring VNA sweep.")
            vna_sweep = vna.sweep_cs(RFfreq, 10e6, num_points=1000, bw=100)
            vna.output_off()
            R = np.sqrt(vna_sweep[:,1]**2 + vna_sweep[:,2]**2)
            RFfreq = vna_sweep[np.argmin(R), 0]
            print("Adjusting RF frequency to {}".format(RFfreq))
            lo.lock()
            rf.frequency(RFfreq)
            lo.frequency(RFfreq)
            lo.unlock()
            np.save(os.path.join(data_dir, "VNA_{}.npy".format(timestamp)), vna_sweep)
            gen.output(True)
            gen.amplitude(noiseA)
            print("Waiting 30 s")
            time.sleep(30)
            for k in range(averages):
                print("Starting {} noise amplitude, {}/{}".format(noiseA, k, averages))
                # f, resp = get_averaged_spectrum(daq, 1)
                try:
                    Ti = lt.wait_for_T3(T3max)
                except:
                    Ti = np.nan
                timeseries = daq.measure()
                try:
                    Tf = lt.get_T3()
                except:
                    Tf = np.nan
                output = {'timeseries': timeseries, 'rate': rate, 'samples': samples, 'fdemod': fdemod,
                          'noise': noiseA, 'RFf': RFfreq, 'tc': tc, 'slope': slope,
                          'Ti': Ti, 'Tf': Tf}
                filename = os.path.join(data_dir, 'TM_{}_{}.npy'.format(k, timestamp))
                np.save(filename, output, allow_pickle=True)
finally:
    lockin.close()
    gen.output(False)
    gen.close()
    # rf.output(False)
    rf.close()
    daq.close()
    vna.close()
    rm_ag.close()