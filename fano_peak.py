"""
Fitting of peaks with complex coherent backgrounds that could result in fano-like shapes.

Created on Wed Jul 24 08:56:15 2019

@author: emil
"""

import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt

Nbparams = 5  # number of background parameters
Npparams = 4  # number of peak parameters


def complex_fano_peak(f, *parameters):
    """
    Complex resonance of a linear harmonic oscillator.

    Parameters
    ----------
    f : array-like
        Frequencies.
    *parameters : array like or tuple
        (A, f0, fwhm, phase).

    Returns
    -------
    array-like of complex
        Resonant response at frequencies **f**.

    """
    A, f0, fwhm, q = parameters
    Z = fwhm*f0/(f0**2 - f**2 - 1j*fwhm*f)*np.exp(-1j*q)
    return A*Z


def fano_peak(f, *parameters):
    """
    Real part of a single comlex linear peak, no background.

    Parameters
    ----------
    f : array-like
        Frequencies.
    *parameters : array like or tuple
        Peak parameters, (A, f0, fwhm, phase).

    Returns
    -------
    array-like
        Real resonant response at frequencies **f**.

    """
    return np.real(complex_fano_peak(f, *parameters))


def bridge_background(f, *p):
    """Background from the bridge detuning."""
    a = p[0]
    b = p[1]
    c = p[2]
    d = p[3]  # unused
    e = p[4]  # unused
    return a + b*f + c/f  # + d*f**2 + e/f**2


def single_fano_peak(f, *parameters):
    """Single peak with background."""
    p1_start = Nbparams
    p1 = parameters[p1_start:]
    return bridge_background(f, *parameters) + fano_peak(f, *p1)


def double_fano_peak(f, *parameters):
    """Double peak with background."""
    p1_start = Nbparams
    p2_start = p1_start + Npparams
    p1 = parameters[p1_start:p2_start]
    p2 = parameters[p2_start:]
    return bridge_background(f, *parameters) + (fano_peak(f, *p1) + fano_peak(f, *p2))
#    return a + b*f + (np.tanh(fano_peak(f, *p1)/c)*c + np.tanh(fano_peak(f, *p2)/d)*d)


class SingleFanoPeak:
    """Fit a single peak with rotated phase."""

    def __init__(self, freq, x, plot=False, p0=None, plot_failed=True, y=None,
                 qguess=0):
        self.plot_failed = plot_failed
        self.freq = freq
        self.x = x
        self.plot = plot
        self.p0 = p0
        self.qguess = qguess
        self.fit()

    def fit(self):
        """Fit the data to a single peak with rotated phase."""
        q = self.qguess
        ix0 = np.argmax(np.abs(self.x))
        A0 = np.abs(self.x[ix0])
        f0 = self.freq[ix0]
        fwhm = f0/1000
        p0 = [0, 0, 0, 0, 0,
              A0, f0, fwhm, q]
        print(p0)
        if self.p0 is not None:
            p0 = self.p0

        try:
            popt, pcov = optim.curve_fit(single_fano_peak, self.freq, self.x, p0=p0,
                                         xtol=1e-8, ftol=1e-8,
                                         maxfev=10000)
            self.popt = popt
#            print('nu = {}'.format(popt[Nbparams+Npparams-1]))
            self.fit_val = single_fano_peak(self.freq, *popt)
        except Exception as e:
            print('Fit failed.', e)
            if self.plot_failed:
                fig, ax = plt.subplots(1, 1)
                ax.plot(self.freq, self.x, label='data')
                ax.plot(self.freq, single_fano_peak(self.freq, *p0), ':', label='initial')
            popt = None
            self.popt = None
            raise

        self.peak_p = popt[Nbparams:]
        self.A0, self.f0, self.fwhm, self.phase = self.peak_p
        self.Q = self.f0/self.fwhm
        self.fwhm = np.abs(self.fwhm)

        if self.plot:
            fig, ax = plt.subplots(1, 1)
            self.fig = fig
            self.ax = ax
            ax.plot(self.freq, self.x, 'o', ms=3, label='data')
            ax.plot(self.freq, single_fano_peak(self.freq, *p0), ':', label='initial')
            if popt is not None:
                ax.plot(self.freq, self.fit_val, label='fit')
                ax.plot(self.freq, fano_peak(self.freq, *self.peak_p), label='peak, no background')
                ax.plot(self.freq, bridge_background(self.freq, *self.popt[:Nbparams]),
                        label='backogrund')
        return popt

    def __call__(self, f):
        """Evaluate the fitted peak."""
        return single_fano_peak(f, *self.popt)

    def background(self, f):
        """
        Calculate the brackground due to the bridge detuning.

        **f**: frequency
        """
        bridge_p = bridge_background(f, *self.popt)

        return bridge_p


class DoubleFanoPeak:
    """Fitting of a spectrum with two peaks."""

    def __init__(self, freq, x, plot=False, p0=None, plot_failed=True, y=None):
        """Fits a double peak to a spectrum."""
        self.plot_failed = plot_failed
        self.freq = freq
        self.x = x
        self.plot = plot
        self.p0 = p0
        self.fit()

    def fit(self):
        """Perform the fit."""
        q = 0
        fmid = 0.5*(self.freq.min() + self.freq.max())
        ix1 = self.freq < fmid
        ix2 = self.freq > fmid
        ix0 = np.argmax(np.abs(self.x))
        A0 = self.x[ix0]
        f0 = self.freq[ix1][np.argmax(np.abs(self.x[ix1]))]
        f02 = self.freq[ix2][np.argmax(np.abs(self.x[ix2]))]
        fwhm = 100
        p0 = [self.x.mean(), 0, 0, 0, 0,
              A0, f0, fwhm, q,
              A0, f02, fwhm, q]

        if self.p0 is not None:
            p0 = self.p0

        try:
            popt, pcov = optim.curve_fit(double_fano_peak, self.freq, self.x, p0=p0,
                                         xtol=1e-8, ftol=1e-8)
            self.popt = popt
#            print('nu = {}'.format(popt[Nbparams+Npparams-1]))
            self.fit_val = double_fano_peak(self.freq, *popt)
        except Exception as e:
            print('Fit failed.', e)
            if self.plot_failed:
                fig, ax = plt.subplots(1, 1)
                ax.plot(self.freq, self.x)
                ax.plot(self.freq, double_fano_peak(self.freq, *p0), ':')
            popt = None
            self.popt = None
            raise

        f0a = popt[Nbparams+1]
        f0b = popt[Nbparams+Npparams+1]
        self.peaks_p = []
        if f0a < f0b:
            self.peaks_p = [popt[Nbparams:(Nbparams+Npparams)],
                            popt[(Nbparams+Npparams):(Nbparams+2*Npparams)]]
        else:
            self.peaks_p = [popt[(Nbparams+Npparams):],
                            popt[Nbparams:(Nbparams+Npparams)]]

        # first peak parameters
        self.A01 = self.peaks_p[0][0]
        self.f01 = self.peaks_p[0][1]
        self.fwhm1 = np.abs(self.peaks_p[0][2])
        self.phase1 = self.peaks_p[0][3]
        # second peak
        self.A02 = self.peaks_p[1][0]
        self.f02 = self.peaks_p[1][1]
        self.fwhm2 = np.abs(self.peaks_p[1][2])
        self.phase2 = self.peaks_p[1][3]

        if self.plot:
            fig, ax = plt.subplots(1, 1)
            self.fig = fig
            self.ax = ax
            ax.plot(self.freq, self.x, 'o', ms=3)
            ax.plot(self.freq, double_fano_peak(self.freq, *p0), ':')
            if popt is not None:
                ax.plot(self.freq, self.fit_val)
                ax.plot(self.freq, fano_peak(self.freq, *self.peaks_p[0]))
                ax.plot(self.freq, fano_peak(self.freq, *self.peaks_p[1]))
                ax.plot(self.freq, bridge_background(self.freq, *self.popt[:Nbparams]))
        return popt

    def background(self, f, background_peak):
        """
        Calculate the background due to the bridge detuning and the other peak.

        **f**: frequency

        **background_peak**: 0 for lower-frequency peak, 1 for higher-frequency peak
        """
        if background_peak >= 0:
            pb = self.peaks_p[background_peak]
            peak_b = fano_peak(f, *pb)
        else:
            peak_b = 0
        bridge_p = bridge_background(f, *self.popt)
        return bridge_p + peak_b
