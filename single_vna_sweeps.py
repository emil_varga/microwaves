# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 11:29:41 2020

@author: Emil
"""

import pyvisa as visa
import matplotlib.pyplot as plt
import numpy as np

import sys
sys.path.append('D:\\ev')
from instruments.vna import VNA

rm = visa.ResourceManager(r"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\agbin\visa32.dll")
vna = VNA(rm, "USB0::0x2A8D::0x5D01::MY54101196::0::INSTR")
# plt.close('all')
try:
    vna.setup(S='S21')
    vna.power(-20)
    
    
    fc = 2.3048e9
    span = 10e6
    fi = fc - span/2
    ff = fc + span/2
    
    data = vna.sweep(fi, ff, num_points=1000, bw=200, avg=None)
    freq = data[:,0]
    x = data[:,1]
    y = data[:,2]
    fig, ax = plt.subplots(1, 1)
    ax.plot(freq, np.sqrt(x**2 + y**2))

finally:
    print("Shutting down instruments")
    vna.output_off()
    vna.close()    
    rm.close()
    