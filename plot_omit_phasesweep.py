# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 17:58:31 2022

@author: emil
"""

import numpy as np
import os.path as path
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

from glob import glob

def detrend(y):
    x = np.arange(len(y))
    p = np.polyfit(x, y, deg=1)
    return y# - np.polyval(p, x)

def rotate(I, Q):
    z = np.abs(I) + 1j*np.abs(Q)
    phi = np.angle(z)
    
    zz = I + 1j*Q
    return zz*np.cos(phi), 1j*zz*np.sin(phi)

base_dir = 'D:/ev/data/microwaves/OMITp'

phasedirs = glob(path.join(base_dir, 'LO*'))
phasedirs.sort()

plt.close('all')
fig, ax = plt.subplots(1, 1)
figrf, axrf = plt.subplots(1, 1)

offset = 0

grid = None

rffreqs = []

for k, phasedir in enumerate(phasedirs[:]):
    files = glob(path.join(phasedir, '*.npy'))
    if len(files) == 0:
        continue
    d = np.load(files[0], allow_pickle=True).item()
    
    axrf.plot(d['cavity'][:,0], np.abs(d['cavity'][:,1] + 1j*d['cavity'][:,2]))    
    
    freq = d['freq']
    I = d['I']
    Q = d['Q']
    

    # In, Qn = rotate(I, Q) 
    
    r = np.abs(Q)
    # ax.plot(freq, detrend(r) + offset)# - savgol_fi1lter(r, 1001, 3))
    # ax.plot(freq, detrend(np.unwrap(np.angle(Q))))
    ax.plot(freq, Q.imag)
    offset += 0.5e-6
