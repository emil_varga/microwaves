# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 11:29:41 2020

@author: Emil
"""

import pyvisa as visa
import time
import os
import numpy as np
import os.path as path

import ltwsclient

import sys
sys.path.append('D:\\ev')
from instruments.vna import VNA

sys.path.append("C:\\code\\labtools")
import cryoutil as cu

rm = visa.ResourceManager(r"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\agbin\visa32.dll")
cc = cu.get_cryocon(rm)
vna = VNA(rm, "USB0::0x2A8D::0x5D01::MY54101196::0::INSTR")
try:
    vna.setup(S='S21')
    vna.power(-20)
    
    print(vna.power())
    
    data_dir = '../data/microwaves/RUN20220720/filled_full/600mK/cooldowns/1'
    
    os.makedirs(data_dir, exist_ok=True)
    
    widesweeps = [(2.2e9, 2.35e9)]

    while True:
        # measure the individual modes
        # for k, f0 in enumerate(f0s):
        for k, (fi, ff) in enumerate(widesweeps):
            print(k, fi, ff)
            timestamp = time.strftime("%Y%m%d-%H%M%S")
            print('Initial temperature:')
            try:
                Ti = cu.get_T3(cc)
            except:
                Ti = np.nan
            print(Ti)
            data = vna.sweep(fi, ff, num_points=10001, bw=1000, avg=None)
            print('Final temperature:')
            try:
                Tf = cu.get_T3(cc)
            except:
                Tf = np.nan    
            print(Tf)
            fn = 'mode_T_{}_{}.npy'.format(k, timestamp)
            output = {'data':data, 'Tstart':Ti, 'Tend':Tf}
            np.save(path.join(data_dir, fn), output)
        # break
finally:
    print("Shutting down instruments")
    vna.output_off()
    vna.close()    
    rm.close()
    