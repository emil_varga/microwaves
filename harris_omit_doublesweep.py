# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 16:50:03 2022

@author: emil
"""

import os
import time
import numpy as np
import pyvisa as visa

import sys
sys.path.append('C:/code/labtools')
import cryoutil as cu

sys.path.append('D:/ev')
from instruments.SR830 import SR830
from instruments.SR844 import SR844
from instruments.DS345 import DS345
from instruments.vna import VNA
from instruments.rfsource import RFsource

base_output_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/OMIT/am_lock/_3dBm_6'

rm = visa.ResourceManager(r"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\agbin\visa32.dll")
cc = cu.get_cryocon(rm)
vna = VNA(rm, "VNA")
Ilia = SR830(rm, 'GPIB0::1::INSTR')
gen = DS345(rm, 'GPIB0::5::INSTR')
lo = RFsource(rm, 'LOsource')
rf = RFsource(rm, 'RFsource')

vna.power(-20)
vna.setup('S21')

RFpower = -3
AMsens = 0.5

LIAtc = '1'
LIAsens = '500u'
LIAslope = '12'

T3max = 0.605
# cavity_centre = 2.30759e9 #Hz
cavity_centre = 2.30735e9
vna_sweep_span = 5e6
#cavity_sweep_span = 0.5e6 #Hz

f_segments = [(1392.6, 1, 0.005)]

wait_time = 3 #s

Ilia.set_timeconstant(LIAtc)
Ilia.set_sensitivity(LIAsens)
Ilia.set_slope(LIAslope)
Ilia.harmonic(1)
# Qlia.harmonic(1)
gen.offset(0)
rf.power(RFpower)
rf.am(True, sens=AMsens)

# RF_frequencies = [2.30689e9, 2.3064e9, 2.3074e9, 2.5e9]
# RF_frequencies = [2.3061e9]
# RF_frequencies = np.linspace(cavity_centre - cavity_sweep_span/2, cavity_centre + cavity_sweep_span/2, 7, endpoint=True)
RF_frequencies = [cavity_centre]
drive_offset = 0#20e3

gen_volts = np.linspace(0.1,1,10, endpoint=True)
# gen_volts = [0.1, 0.2, 0.5, 1, 2]

lo.output(True)
rf.output(True)
was_warm = False
while True:
    for RF_freq in RF_frequencies:
        print("Starting RF f {:.2f}".format(RF_freq))
        data_dir = os.path.join(base_output_dir, 'RF_{:.0f}'.format(RF_freq))
        os.makedirs(data_dir, exist_ok=True)
        lo.lock()
        lo.frequency(RF_freq)
        lo.unlock()
        rf.frequency(RF_freq)
        # rfmixed.frequency(RF_freq+drive_offset)
        
        print("waiting 2 min for homdyne lock")
        time.sleep(120)
        for ampl in gen_volts:            
            gen.amplitude(ampl, unit='VP')
            print(f"Starting drive voltage {ampl} Vpp.")
            amp_dir = os.path.join(data_dir, 'AMPL_{:.4f}'.format(ampl))
            os.makedirs(amp_dir, exist_ok=True)
            
            while True:
                try:
                    T3 = cu.get_T3(cc)
                    if T3 < T3max:
                        if was_warm:
                            print("First cooldown, waiting 3 min")
                            was_warm = False
                            time.sleep(180)
                        break
                    print("T3 = {}".format(T3))
                    was_warm = True
                except:
                    print("Couldn't read T3, trying again.")
                time.sleep(5)
            t0 = time.time()
            print("taking vna sweep")
            vna_sweep = vna.sweep_cs(cavity_centre, vna_sweep_span, bw=200, num_points=1000)
            vna.output_off()
            time.sleep(30)
            # vna_sweep = None
            for fc, fs, step in f_segments:
                Is = []
                Ts = []
                ts = []
                timestamp = time.strftime("%Y%m%d-%H%M%S")
                frequencies = np.arange(fc - fs/2, fc + fs/2+step, step)
                for f in frequencies:
                    print("Starting {} Hz".format(f))
                    gen.frequency(f)
                    time.sleep(wait_time)
                    while True:
                        try:
                            T3 = cu.get_T3(cc)
                            if T3 < T3max:
                                if was_warm:
                                    print("First cooldown, waiting 3 min")
                                    was_warm = False
                                    time.sleep(180)
                                break
                            print("T3 = {}".format(T3))
                            was_warm = True
                        except:
                            print("Couldn't read T3, trying again.")
                        time.sleep(5)
                    Ilia.lock()
                    Ix, Iy = Ilia.get_xy()
                    Ilia.unlock()
                    t = time.time()
                    Is.append(Ix + 1j*Iy)
                    Ts.append(T3)
                    ts.append(t - t0)
                
                Is = np.array(Is)
                Ts = np.array(Ts)
                ts = np.array(ts)
                
                output = {'freq': frequencies, 'I': Is, 'T3': Ts, 'cavity': vna_sweep,
                          'time': ts, 'RFpower': RFpower, 'AMsens': AMsens, 'AMampl': ampl,
                          'LIAtc': LIAtc, 'LIAsens': LIAsens, 'LIAslope': LIAslope}
                np.save(os.path.join(amp_dir, 'OMIT_{}.npy'.format(timestamp)), output)