#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:01:02 2019

@author: emil
"""

import numpy as np
import visa
from datetime import datetime
import os.path as path
import os
import ltwsclient as ltwsc

from instruments.SR830 import SR830
from instruments.SG384 import SG384
from instruments.DS345 import DS345
from instruments.DAQcard import DAQcard

from numpy.random import default_rng
rng = default_rng()

device_id = 'dev538'
continuous = True

# output_dir = '../data/RUN_20200917/mechanics/RFvariable/SPRINGMECH85kHz_finer'
output_dir = '../data/RUN_20200917/mechanics/RF2.72356/THERMOMECH85kHz_1amp'
f0 = 85e3 # Hz
DAQrate = 16384 # Hz
DAQsamples = 16*DAQrate # this should give frequency resolution 1/16 Hz
time_constant = '10u'
lockin_sensitivity = '1m'
navg = 5

RFcent = 2.72356e9 # Hz
RFspan = 0.5e6 # Hz
RFsamp = 500
RFfreqs = [RFcent]
# RFfreqs = np.linspace(RFcent-RFspan/2, RFcent+RFspan/2, RFsamp)
# rng.shuffle(RFfreqs)

T3max = 0.5

if __name__ == '__main__':
    os.makedirs(output_dir,exist_ok=True)

    # data readout from logger
    server = "onnes.ccis.ualberta.ca"
    port = "3172"
    print(f"connecting to {server}:{port}")
    lt = ltwsc.LTWebsockClient(server, port)
    
    rm = visa.ResourceManager()
    print('Connecting to lockin.')
    # lockin = zi.ziLockin(device_id)
    lockin = SR830(rm, 'GPIB0::1::INSTR')
    lockin.set_timeconstant(time_constant)
    lockin.set_sensitivity(lockin_sensitivity)
    
    print('Connecting to RF source.')
    rf = SG384(rm, 'GPIB0::27::INSTR')
    rf.power(16)
    rf.output(True)
    
    print('Connecting to generator')
    gen = DS345(rm, 'GPIB0::2::INSTR')
    gen.frequency(f0)
    gen.amplitude(0.01, 'VP')
    
    print('Setting up DAQ card.')
    daq = DAQcard(channels=['ai1', 'ai2'], rate=DAQrate, samples=DAQsamples)
    
    not_done = True
    try:
        while not_done:
            for rffreq in RFfreqs:
                print(f'Starting {rffreq/1e9} GHz')
                rf.frequency(rffreq)
                n = 0
                data_dir = path.join(output_dir, 'RF{:.6f}'.format(rffreq/1e9))
                os.makedirs(data_dir, exist_ok=True)
                while n < navg:
                    print(f'Sample {n}/{navg}, {rffreq/1e9} GHz')
                    try:
                        T3 = lt.wait_for_T3(T3max)
                    except:
                        print('Couldnt get start temperature.')
                        continue
                    
                    now = datetime.now()
                    timestamp = now.strftime('%Y%m%d-%H_%M_%S')
                    # data = lockin.daq_continuous(f0, time_constant, DAQrate, DAQsamples)
                    xy = daq.measure()
                    data = {}
                    data['x'] = xy[0, :]
                    data['y'] = xy[1, :]
                    data['_RF_freq'] = rffreq
                    data['_Tstart'] = T3
                    data['_rate'] = DAQrate
                    data['_samp'] = DAQsamples
                    data['_demod_f0'] = f0
                    try:
                        T3end = lt.get_T3()
                    except:
                        print('Couldnt get end temperature.')
                        continue
                    data['_Tend'] = T3end
                    filename = 'TS-'+timestamp
                    np.save(path.join(data_dir, filename), data)
                    if data['_Tend'] < T3max+0.1:
                        n += 1
            if not continuous:
                not_done = False
    finally:
        daq.close()
        rf.output(False)
        rf.close()
        gen.close()
        lockin.close()
        rm.close()