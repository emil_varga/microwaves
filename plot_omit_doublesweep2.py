# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 17:58:31 2022

@author: emil
"""

import matplotlib as mp
mp.rcParams['font.size'] = 12
mp.rcParams['axes.spines.top'] = False
mp.rcParams['axes.spines.right'] = False

import numpy as np
import os.path as path
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

from glob import glob

def detrend(y):
    x = np.arange(len(y))
    p = np.polyfit(x, y, deg=1)
    return y# - np.polyval(p, x)

def rotate(I, Q):
    z = np.abs(I) + 1j*np.abs(Q)
    phi = np.angle(z)
    
    zz = I + 1j*Q
    return zz*np.cos(phi), 1j*zz*np.sin(phi)

def dbm2V(dbm):
    Z0 = 50
    W = 10**(dbm/10)/1000
    V = np.sqrt(Z0*W)
    return V

# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/buzzer/6'
# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/OMIT/pm_lock/_3dBm_0'
base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/OMIT/am_lock/_3dBm_6'
# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/OMIT/am_lock_old/6'

rfdirs = glob(path.join(base_dir, 'RF*'))
rfdirs.sort()

plt.close('all')
figrf, axrf = plt.subplots(2, 1, sharex=True)
# figp, axp = plt.subplots(1, 1)

fig_all, ax_all = plt.subplots(1, 1)

offset = 0

rffreqs = []

for k, rfdir in enumerate(rfdirs[:]):
    amdirs = glob(path.join(rfdir, 'AMPL*'))
    for amdir in amdirs:
        avg_peak = 0
        avg_x = 0
        avg_y = 0
        files = glob(path.join(amdir, '*.npy'))
        if len(files) == 0:
            continue
        print(rfdir, amdir)
        fig_xy, ax_xy = plt.subplots(2, 1, sharex=True)
        for file in files:
            d = np.load(file, allow_pickle=True).item()
            
            rffreq = float(path.split(rfdir)[-1][3:])
            rffreqs.append(rffreq)
            
            if d['cavity'] is not None:
                f = d['cavity'][:,0]
                vnax = d['cavity'][:,1]
                vnay = d['cavity'][:,2]
                A = np.abs(vnax + 1j*vnay)
                phi = np.angle(vnax + 1j*vnay)
                delayp = np.polyfit(f, phi, deg=1)
                axrf[0].plot(f, A)
                axrf[1].plot(f, phi - np.polyval(delayp, f))
                axrf[0].axvline(rffreq)
                axrf[1].axvline(rffreq)
                figrf.tight_layout()
            
            freq = d['freq']
            I = d['I']
                    
            # ax.plot(freq, np.abs(I))
            avg_peak += np.abs(I)
            avg_x += I.real
            avg_y += I.imag
            ax_xy[0].plot(freq, I.real)
            ax_xy[1].plot(freq, I.imag)
            
        fig_avg, ax_avg = plt.subplots(2, 1, sharex=True, sharey=False)
        ax_avg[1].set_xlabel('AM frequency (Hz)')
        ax_avg[1].set_ylabel('y-homodyne signal')
        ax_avg[0].set_ylabel('x-homodyne signal')
        # ax_avg.plot(freq, avg_peak/len(files))
        # ax_avg.plot(freq, np.sqrt(avg_x**2 + avg_y**2)/len(files))
        ax_avg[0].plot(freq, avg_x/len(files), '-o', ms=2)
        ax_avg[1].plot(freq, avg_y/len(files), '-o', ms=2)
        ax_avg[0].set_title('avg '+path.split(rfdir)[-1] + ' ' + path.split(amdir)[-1])
        fig_avg.tight_layout()
        
        avg_x /= len(files)
        avg_y /= len(files)
        
        ax_all.plot(freq, avg_y)
        # fig_avg_mag, ax_avg_mag = plt.subplots(1, 1)
        # ax_avg_mag.set_title(path.split(amdir)[-1])
        # ax_avg_mag.plot(freq, abs(avg_x + 1j*avg_y), '-o')
        # ax_avg_mag.set_xlabel(r'$\Omega/2\pi$ (Hz)')
        # ax_avg_mag.set_ylabel(r'OMIT/OMIA (a.u.)')
        # fig_avg_mag.tight_layout()
figrf.tight_layout()

ax_all.set_xlabel('frequency (Hz)')
ax_all.set_ylabel('OMIT y')
fig_all.tight_layout()