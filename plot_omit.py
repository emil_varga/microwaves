# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 09:26:01 2022

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

def detrend(y):
    x = np.arange(len(y))
    p = np.polyfit(x, y, deg=3)
    return y - np.polyval(p, x)


plt.close('all')

# file = r'D:\ev\data\microwaves\RUN20220707\omit\fsweep100.npy'
# d = np.load(file, allow_pickle=True).item()
# ax[0].plot(d['freq'][5:], d['I'][5:].real, '-o', ms=2)
# ax[1].plot(d['freq'][5:], d['I'][5:].imag, '-o', ms=2)


figRI, axRI = plt.subplots(2, 1, sharex=True)
figRID, axRID = plt.subplots(2, 1, sharex=True)
figRP, axRP = plt.subplots(2, 1, sharex=True)

file = r'D:\ev\data\microwaves\RUN20220720\omit\fsweep8.npy'
d = np.load(file, allow_pickle=True).item()
axRI[0].plot(d['freq'][5:], d['I'][5:].real, '-o', ms=2)
axRI[1].plot(d['freq'][5:], d['I'][5:].imag, '-o', ms=2)

# ax[0].plot(d['freq'][5:], d['Q'][5:].real, '-o', ms=2)
# ax[1].plot(d['freq'][5:], d['Q'][5:].imag, '-o', ms=2)

# axRID[0].plot(d['freq'][5:], detrend(d['I'][5:].real), '-o', ms=2)
# axRID[1].plot(d['freq'][5:], detrend(d['I'][5:].imag), '-o', ms=2)
axRID[0].plot(d['freq'][5:], savgol_filter(detrend(d['I'][5:].real), 11, 3), '-o', ms=2)
axRID[1].plot(d['freq'][5:], savgol_filter(detrend(d['I'][5:].imag), 11, 3), '-o', ms=2)

# ax[0].plot(d['freq'][5:], detrend(d['Q'][5:].real), '-o', ms=2)
# ax[1].plot(d['freq'][5:], detrend(d['Q'][5:].imag), '-o', ms=2)

axRP[0].plot(d['freq'][5:], np.abs(d['I'][5:]), '-o', ms=2)
axRP[1].plot(d['freq'][5:], np.angle(d['I'][5:]), '-o', ms=2)

# ax[0].plot(d['freq'][5:], np.abs(d['Q'][5:]), '-o', ms=2)
# ax[1].plot(d['freq'][5:], np.angle(d['Q'][5:]), '-o', ms=2)


file = r'D:\ev\data\microwaves\RUN20220720\omit\fsweep9.npy'
d = np.load(file, allow_pickle=True).item()
axRI[0].plot(d['freq'][5:], d['I'][5:].real, '-o', ms=2)
axRI[1].plot(d['freq'][5:], d['I'][5:].imag, '-o', ms=2)

# ax[0].plot(d['freq'][5:], d['Q'][5:].real, '-o', ms=2)
# ax[1].plot(d['freq'][5:], d['Q'][5:].imag, '-o', ms=2)

# axRID[0].plot(d['freq'][5:], detrend(d['I'][5:].real), '-o', ms=2)
# axRID[1].plot(d['freq'][5:], detrend(d['I'][5:].imag), '-o', ms=2)
axRID[0].plot(d['freq'][5:], savgol_filter(detrend(d['I'][5:].real), 11, 3), '-o', ms=2)
axRID[1].plot(d['freq'][5:], savgol_filter(detrend(d['I'][5:].imag), 11, 3), '-o', ms=2)

# ax[0].plot(d['freq'][5:], detrend(d['Q'][5:].real), '-o', ms=2)
# ax[1].plot(d['freq'][5:], detrend(d['Q'][5:].imag), '-o', ms=2)

axRP[0].plot(d['freq'][5:], np.abs(d['I'][5:]), '-o', ms=2)
axRP[1].plot(d['freq'][5:], np.angle(d['I'][5:]), '-o', ms=2)

# ax[0].plot(d['freq'][5:], np.abs(d['Q'][5:]), '-o', ms=2)
# ax[1].plot(d['freq'][5:], np.angle(d['Q'][5:]), '-o', ms=2)
