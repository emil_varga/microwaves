# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 16:50:03 2022

@author: emil
"""

import time
import numpy as np
import pyvisa as visa

import sys
sys.path.append('C:/code/labtools')
import cryoutil as cu

sys.path.append('D:/ev')
from instruments.SR844 import SR844
from instruments.SR830 import SR830
from instruments.DS345 import DS345
from instruments.rfsource import RFsource

rm = visa.ResourceManager()

cc = cu.get_cryocon(rm)
Ilia = SR830(rm, 'GPIB0::1::INSTR')
Qlia = SR830(rm, 'GPIB0::2::INSTR')
gen = DS345(rm, 'GPIB0::5::INSTR')
lo = RFsource(rm, 'LOsource')
rf = RFsource(rm, 'RFsource')
rfmixed = RFsource(rm, 'RFsource_mixed')

rf_centre = 2.33455e9

rf.frequency(rf_centre)
rf.output(False)

lo.frequency(rf_centre)
rfmixed.frequency(rf_centre)

gen.offset(0.1)
gen.amplitude(-20, 'DB')

lo.output(True)
rfmixed.output(True)
lo.phase(0)

f_start = 1000 #Hz
f_stop = 100000 #Hz
f_step =  10#Hz
Ilia.set_timeconstant("10m")
Qlia.set_timeconstant("10m")
wait_time = 0.1 #s

frequencies = np.arange(f_start, f_stop, f_step)
Is = []
Qs = []
Ts = []
# gen.amplitude(1, 'VP')
for f in frequencies:
    print("Starting {} Hz".format(f))
    gen.frequency(f)
    time.sleep(wait_time)
    while True:
        try:
            T3 = cu.get_T3(cc)
            if T3 < 0.55:
                break
            print("T3 = {}".format(T3))
        except:
            print("Couldn't read T3, trying again.")
        time.sleep(5)
    Ilia.lock()
    Ix, Iy = Ilia.get_xy()
    Ilia.unlock()
    Qlia.lock()
    Qx, Qy = Qlia.get_xy()
    Qlia.unlock()
    Is.append(Ix + 1j*Iy)
    Qs.append(Qx + 1j*Qy)
    Ts.append(T3)
# gen.amplitude(0.01, 'VP')
Is = np.array(Is)
Qs = np.array(Qs)
Ts = np.array(Ts)

output = {'freq': frequencies, 'I': Is, 'Q': Qs, 'T3': Ts}
np.save('../data/microwaves/RUN20220720/omit/fsweep9.npy', output)

# lo.output(False)
# rf.output(False)