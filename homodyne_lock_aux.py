# -*- coding: utf-8 -*-
"""
Created on Thu Jul  7 14:41:02 2022

@author: Emil
"""

import pyvisa as visa
import numpy as np
import time

import sys
sys.path.append('D:/ev/')
from instruments.SR830 import SR830
from instruments.rfsource import RFsource

sys.path.append("C:\\code\\labtools")
import cryoutil as cu

P = 5000
I = 2000
T3max = np.inf#0.605

rm = visa.ResourceManager()
lockin = SR830(rm, 'GPIB0::2::INSTR')
lo = RFsource(rm, 'LOsource')
rf = RFsource(rm, 'RFsource')
cc = cu.get_cryocon(rm)

olderror = 0
error = 0
integral = 0
told = time.time()
t0 = told
lo.lock()
old_frequency = lo.frequency()
lo.unlock()
try:
    while True:
        t = time.time()
        if cu.get_T3(cc) > T3max:
            told = t
            print("Too hot")
            time.sleep(5)
            integral=0
            continue
        olderror = error
        lockin.lock()
        error, _ = lockin.get_xy()
        lockin.unlock()
        
        integral = integral + 0.5*(t - told)*(error + olderror)
        new_phase = P*error + I*integral
        lo.lock()
        lo.phase(new_phase)
        set_phase = lo.phase()
        frequency = lo.frequency()
        lo.unlock()
        print('t-t0 = {:.3f} s: phi={:.3f} err={:.3e}, integral={:.3e}'.format(t-t0, set_phase, error, integral))
        if new_phase > 2*np.pi or abs(frequency - old_frequency) > 1000:
            print("Winding phase too much or frequency changed by more than 1 kHz, resetting.")
            olderror=0
            integral=0
            lo.lock()
            lo.phase(0)
            lo.unlock()
        time.sleep(0.1)
        old_frequency = frequency
        told = t
finally:
    lockin.close()
    lo.close()
    rf.close()
    rm.close()