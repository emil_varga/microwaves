# -*- coding: utf-8 -*-
"""
Created on Sun Dec 20 12:00:24 2020

@author: emil
"""

import numpy as np
import visa

import sys
sys.append.path("D:\\ev")
from instruments.vna import VNA
from instruments.rfsource import BNC865

import ltwsclient as ltwsc
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

RFspan = 2e6 #Hz
RFstep = 1e4 #Hz
RFcent = 5.364610220e9

rm = visa.ResourceManager('C:\\Program Files (x86)\\IVI Foundation\\VISA\\WinNT\\agvisa\\agbin\\visa32.dll')

vna = VNA(rm, 'USB0::0x2A8D::0x5D01::MY54301840::0::INSTR')
rf = BNC865(rm, 'USB0::0x03EB::0xAFFF::471-4396D0600-1250::0::INSTR')

RFf = RFcent - RFspan/2
while 

