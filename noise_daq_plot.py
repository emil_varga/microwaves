# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 15:06:14 2022

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
import scipy.ndimage as img

import os.path as path
from glob import glob

import MicrowavePeak as mwp

plt.close('all')

def filter_response(f, tau, n=2):
    s = (1/(1 + 1j*(2*np.pi)*f*tau))**n
    return abs(s)

base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/spring_noise/pm_lock/4'
# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/spring_noise/0dBm/pm_lock/1'
# base_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/600mK/spring_noise/pm_lock/5'
resolution = 0.01 #Hz
plot_peaks = False

rfdirs = glob(path.join(base_dir, 'RF*'))
rfdirs.sort()

rffreqs = []
grid = []
delta = []
fcs = []
for rfdir in rfdirs:
    files = glob(path.join(rfdir, '*.npy'))
    Sxx_avg = 0
    print("{}: {}".format(rfdir, len(files)))
    if len(files) == 0:
        continue
    if plot_peaks:
        fig, (ax, axrf) = plt.subplots(1, 2)
    # figL, axL = plt.subplots(1, 1)
    N = 0
    fcavity_avg = []
    for file in files:
        d = np.load(file, allow_pickle=True).item()
        rate = d['rate']
        samples = d['samples']
        data = d['data']
        x = data[0,:]
        y = data[1,:]
        z = x + 1j*y
        # z -= z.mean()
                
        f, Sxx = sig.welch(z, fs=rate, return_onesided=False, nperseg=int(rate/resolution))
        Sxx /= filter_response(f, 30e-3)**2
        
        if plot_peaks:
            ax.semilogy(f + d['LIAfreq'], Sxx)
        # axL.plot(data[2,:])
        # if max(abs(data[2,:])) > 0.1:
        #     axL.plot(data[2,:], color='k')
        #     # continue
        # else:
        #     axL.plot(data[2,:])
        Sxx_avg += Sxx
        N += 1
        if 'vna_sweep' in d:
            rff = d['vna_sweep'][:,0]
            rfx = d['vna_sweep'][:,1]
            rfy = d['vna_sweep'][:,2]
            fd = d['RFfreq']
            dw = 100e3
            peak = mwp.MicrowavePeak(rff, rfx, rfy, mask=[(fd-dw, fd+dw),(fd-dw-5e6, fd+dw-5e6)])
            peak.fit()
            if plot_peaks:
                pl = axrf.plot(rff, np.abs(rfx + 1j*rfy), 'o', ms=2)
                axrf.plot(peak.f*1e9, np.abs(peak.out.best_fit), '-', color=pl[-1].get_color())
                axrf.axvline(d['RFfreq'])
                axrf.axvline(peak.out.best_values['f0']*1e9, ls='--')
            fcavity_avg.append(peak.out.best_values['f0']*1e9)       
        else:
            fcavity_avg.append(0)
    
    delta.append(d['RFfreq'] - np.mean(fcavity_avg))
    fcs.append(np.mean(fcavity_avg))
        # fig, ax = plt.subplots(1, 1)
        # ax.set_title(file)
        # ax.plot(f, Sxx)
    if plot_peaks:
        ax.set_title(path.split(rfdir)[-1])
    # ax.set_ylim(1e-4, 2)
    
        
    if N==0:
        continue
    rffreqs.append(d['RFfreq'])
    
    if plot_peaks:
        ax.plot(f + d['LIAfreq'], Sxx_avg/N, 'k')
        fig.tight_layout()
    
    gridline = np.fft.fftshift(Sxx_avg/N)
    grid.append(gridline)#/np.max(gridline))

print("RF dirs: {}".format(len(rfdirs)))

grid = np.array(grid)
rffreqs = np.array(rffreqs)
fig, ax = plt.subplots(1, 1)
delta = np.array(delta)
frequencies = np.fft.fftshift(f) + d['LIAfreq']
ix = np.logical_and(frequencies > 1392.2,
                    frequencies < 1393)
ax.pcolormesh(frequencies[ix], delta/1e6, img.gaussian_filter(np.log(grid[:,ix]), (0.5, 2)), shading='auto')
# ax.imshow(np.log(grid[:,ix]), aspect='auto', interpolation='gaussian', origin='lower')
ax.set_xlabel(r'$\Omega / 2\pi$ (Hz)')
ax.set_ylabel(r'$\Delta$ (MHz)')
# ax.set_xlim(1391, 1394)
fig.tight_layout()

fig, ax = plt.subplots(1, 1)
ax.plot(delta, fcs, '-o')