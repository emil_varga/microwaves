# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

import os
from glob import glob

data_dir = '../data/microwaves/RUN20220707/empty_ampm_stepped/thermomechanics/T0'

plt.close('all')
fig, ax = plt.subplots(1, 1)

files = glob(os.path.join(data_dir, 'TM*.npy'))
for file in files[:6]:
    meas = np.load(file, allow_pickle=True).item()
    data = meas['timeseries']
    cdata = data[0, :] + 1j*data[1,:]
    cdata = cdata - np.mean(cdata)
    # cdata /= np.std(cdata)
    # print(data.shape)
    fft = np.fft.fftshift(np.fft.fft(cdata, n=len(cdata)))
    freqs = np.fft.fftshift(np.fft.fftfreq(meas['samples'], 1/meas['rate'])) #+ meas['fdemod']
    ix = abs(freqs) < 500
    ax.plot(freqs[ix] + meas['fdemod'], np.abs(fft[ix])**2)

