# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: emil
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from scipy.ndimage import gaussian_filter

import sys
sys.path.append("D:\\ev")
import instruments.SR830 as SR830
from instruments.KS33210A import KS33210A
from instruments.rfsource import BNC865
from instruments.SG384 import SG384
from instruments.DS345 import DS345
from instruments.vna import VNA

def find_f0(vna_sweep):
    f = vna_sweep[:,0]
    x = vna_sweep[:,1]
    y = vna_sweep[:,2]
    r = np.abs(x + 1j*y)
    rs = gaussian_filter(r, 10)
    return f[np.argmin(rs)]

import ltwsclient as ltwsc
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

T3t = 0.8
T3_tolerance = 0.005
# T3_tolerance = 5
check_T_every = 5 #s
check_vna_every = 4 #sweeps

output_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T800/blue/0/'

chunk_size = 300
df = 1
# fmin = 1220
# fmax = 1221.4
# fmin = 2847
# fmax = 2849
fmin = 2950
fmax = 10000

freqs = []
fi = fmin
while fi < fmax:
    freqs.append((fi, fi+chunk_size*df, chunk_size))
    fi += chunk_size*df

print("Measuring in ranges:")
for f in freqs:
    print("\t"+str(f))
    

harmonics = [1]

drive_powers = [-10]
RFfreq = 5.315155e9 #Hz. for 0.8 K
# RFfreq = 5.3152e9 #Hz. for 0.8 K, slightly blue
# RFfreq = 5.313964618e9 #Hz, for 0.9 K
# RFfreq = 5.31405e9 #Hz, for 0.9 K, slightly blue
# RFfreq = 5.315713991e9 #Hz for 0.7 K
sign = 1 # 1 for blue, -1 for red 
detune = 0 # Hz, detune from RFfreq by this much
RFspan = 10e6 #Hz
RFpoints = 1000
RFpower = 18 #dBm
IFBW = 300 #Hz
#in addition to temperature check, check that the resonance is actually at RFfreq
#do not use together with adjust_RF
vna_T_check = True
RFfreq_tolerance = 0.1e6 # tolerance for vna T check
adjust_sensitivity = True

reps = 1000

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()
vna = VNA(rm, 'USB0::0x0957::0x1509::MY51200747::INSTR')

lockinI = SR830.SR830(rm, 'GPIB0::1::INSTR')
lockinI.clear()
lockinQ = SR830.SR830(rm, 'GPIB0::2::INSTR')
lockinQ.clear()

lia_sens = 100e-3
for lia in [lockinI, lockinQ]:
    lia.coupling('ac')
    lia.set_sensitivity('100m')
    lia.set_timeconstant('30m')
    lia.set_slope('12')
    lia.set_reference('external')
    lia.set_reserve('normal')
    lia.harmonic(1)

wait_time = 0.1 # s

# generator = KS33210A(rm, '33210A')
generator = DS345(rm, 'GPIB0::5::INSTR')
generator.amplitude(0.1)
generator.output(True)

rf = BNC865(rm, 'BNC845')
rf.frequency(RFfreq + detune)
rf.power(RFpower)
rf.output(True)

drive = SG384(rm, 'GPIB0::3::INSTR')
drive.frequency(RFfreq + detune + sign*100)
drive.power(drive_powers[0])
since_last_vna_check = np.inf
last_vna_timestamp = None
try:
    r = 0
    while r < reps:
        r += 1
        for lh in harmonics:
            lockinI.harmonic(lh)
            lockinQ.harmonic(lh)
            for DP in drive_powers:
                drive.power(DP)
                n=0
                while n < len(freqs):
                    fr = freqs[n]
                    fs = np.linspace(fr[0], fr[1], fr[2])
                    # np.random.shuffle(fs)
                    print(fs[0])
                    Ixs, Iys = [], []
                    Qxs, Qys = [], []
                    fs_r = []
                    ts = []
                    T3s = []
                    
                    while True:
                        try:
                            T3i = lt.get_T3()
                        except:
                            T3i = np.nan
                        if abs(T3i - T3t) < T3_tolerance:
                            print('')
                            break
                        print("Waiting for T3={:.3f} to reach {:.3f}\r".format(T3i, T3t), end='')
                        unbalanced = True
                        since_last_vna_check = np.inf
                        time.sleep(1)
                    print("Starting {} Hz, {} dBm".format(fr,DP))
                    
                    if since_last_vna_check > check_vna_every:
                        print("Getting microwave sweep")
                        vna_sweep = vna.sweep_cs(RFfreq, RFspan, RFpoints, IFBW)
                        since_last_vna_check = 0
                        vna.output_off()
                   
                    #check whether the peak is where we want it
                    if vna_T_check:
                        f0 = find_f0(vna_sweep)
                        if abs(f0 - RFfreq) > RFfreq_tolerance:
                            #try again if it's too far
                            print("Resonance frequency {:.3f} MHz too far from expected {:.3f} MHz"
                                  " (delta={:.3f} MHz)".format(f0/1e6, RFfreq/1e6, (RFfreq-f0)/1e6))
                            print("Trying again")
                            unbalanced = True
                            since_last_vna_check = np.inf
                            time.sleep(1)
                            continue
                    
                    timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    if since_last_vna_check == 0:
                        last_vna_timestamp = timestamp
                    generator.frequency(fs[0]) # set the first frequency
                    drive.frequency(RFfreq + detune + sign*fs[0])
                    fstr = generator.frequency()
                    rmax = 0
                    t0 = -np.inf
                    aborted = False
                    nans = 0
                    maxnans = 5
                    for k in range(len(fs)):
                        t = time.time()
                        if t - t0 > check_T_every:
                            t0 = t
                            try:
                                T3 = lt.get_T3()                                
                            except:
                                T3 = np.nan
                            if np.isnan(T3):
                                nans += 1
                                print("Couldn't get temperature!")
                                if nans > maxnans:
                                    print("Too many nans in temperature, aborting.")
                                    aborted = True
                                    break
                            elif abs(T3 - T3t) > T3_tolerance:
                                print("Lost temperature control, aborting and redoing.")
                                aborted = True
                                break
                            else:
                                nans = 0
                                T3s.append((t, T3))
                        print("\r{}/{}".format(k, len(fs)), end='')
                        freq = fs[k]
                        generator.frequency(freq)
                        drive.frequency(RFfreq + detune + sign*freq)
                        time.sleep(wait_time)
                        while True:
                            Ix, Iy = lockinI.get_xy()
                            Qx, Qy = lockinQ.get_xy()
                            # Qx, Qy = 0, 0
                            # Ix, Iy = 0, 0
                            Ir = np.sqrt(Ix**2 + Iy**2)
                            Qr = np.sqrt(Qx**2 + Qy**2)
                            if max(Ir, Qr) > rmax:
                                rmax = max(Ir, Qr)
                            if adjust_sensitivity:
                                overload = Ir > lia_sens or Qr > lia_sens
                                if overload:
                                    print("Lockin overloading!")
                                if rmax < 0.1*lia_sens or overload:
                                    print("Changing lockin sensitivity ({:.6f}/{:.6f}), overload={}".format(r, lia_sens, overload))
                                    lia_sens = lockinI.auto_sens(rmax)
                                    lockinQ.auto_sens(rmax)
                                    time.sleep(0.5)
                                    continue
                            break
                        fstr = generator.frequency()
                        fs_r.append(float(fstr))
                        Ixs.append(Ix)
                        Iys.append(Iy)
                        Qxs.append(Qx)
                        Qys.append(Qy)
                        ts.append(t)
                    since_last_vna_check += 1
                    
                    t = time.time()
                    try:
                        T3 = lt.get_T3()
                    except:
                        T3 = np.nan
                    T3s.append((t, T3))

                    if aborted or abs(T3s[-1][1] - T3t) > T3_tolerance:
                        print("Lost temperature control, re-doing.")
                        since_last_vna_check = np.inf
                        continue
                    data = np.column_stack((np.array(fs_r), Ixs, Iys, Qxs, Qys, ts))
                    print(data.shape)
                    out = {'drive_power_dbm': DP, 'data': data, 'RF_freq': RFfreq+detune,
                           'temperatures': T3s, 'T3t': T3t, 'harmonic': lh,
                           'vna_sweep': vna_sweep, 'RFpower': RFpower,
                           'last_vna_sweep': last_vna_timestamp}
                    np.save(path.join(output_dir, 'FS_SR_'+timestamp), out, allow_pickle=True)
                    n+=1
                    # i+=1
                    # if i > 10:
                    #     i=0
finally:
    lockinI.close()
    lockinQ.close()
    generator.close()
    rf.close()
    drive.close()
    rm.close()
    vna.close()