# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 16:50:03 2022

@author: emil
"""

import os
import time
import numpy as np
import pyvisa as visa

import sys
sys.path.append('C:/code/labtools')
import cryoutil as cu

sys.path.append('D:/ev')
from instruments.SR830 import SR830
from instruments.SR844 import SR844
from instruments.DS345 import DS345
from instruments.vna import VNA
from instruments.rfsource import RFsource

base_output_dir = 'D:/ev/data/microwaves/OMITp/'

rm = visa.ResourceManager(r"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\agbin\visa32.dll")
cc = cu.get_cryocon(rm)
vna = VNA(rm, "USB0::0x2A8D::0x5D01::MY54101196::0::INSTR")
# Ilia = SR830(rm, 'GPIB0::1::INSTR')
Qlia = SR844(rm, 'GPIB0::3::INSTR')
gen = DS345(rm, 'GPIB0::5::INSTR')
lo = RFsource(rm, 'LOsource')
rf = RFsource(rm, 'RFsource')
probegen = DS345(rm, 'GPIB0::6::INSTR')

vna.power(-20)
vna.setup('S21')

probegen.offset(0.2)

rf_f = 5.307e9 #Hz

lo_phase_start = 0
lo_phase_stop = np.pi
lo_phase_step = np.pi/10

f_start = 110000 #Hz
f_stop = 180000 #Hz
f_step = 10 #Hz

cavity_centre = 5.307e9 #Hz
cavity_sweep_span = 7e6 #Hz

wait_time = 0.1 #s

# Ilia.set_timeconstant('30m')
Qlia.set_timeconstant('30m')
# Ilia.set_slope('12')
Qlia.set_slope('12')
lo.frequency(rf_f)
rf.frequency(rf_f)
rf.phase(0)

lo_phases = np.arange(lo_phase_start, lo_phase_stop, lo_phase_step)
frequencies = np.arange(f_start, f_stop, f_step)

for lo_phase in lo_phases:
    data_dir = os.path.join(base_output_dir, 'LO_{:.3f}rad'.format(lo_phase))
    os.makedirs(data_dir, exist_ok=True)
    
    lo.lock()
    lo.phase(lo_phase)
    lo.unlock()
    gen.frequency(frequencies[0])
    
    t0 = time.time()
    timestamp = time.strftime("%Y%m%d-%H%M%S")
    vna_sweep = vna.sweep_cs(cavity_centre, cavity_sweep_span, bw=2e3)
    vna.output_off()
    
    #give it some time for the homodyne PID to lock
    time.sleep(30)
    
    
    Is = []
    Qs = []
    Ts = []
    ts = []
    for f in frequencies:
        print("Starting {} Hz".format(f))
        gen.frequency(f)
        time.sleep(wait_time)
        while True:
            try:
                T3 = cu.get_T3(cc)
                if T3 < 0.5:
                    break
                print("T3 = {}".format(T3))
            except:
                print("Couldn't read T3, trying again.")
            time.sleep(5)
        # Ilia.lock()
        # Ix, Iy = Ilia.get_xy()
        # Ilia.unlock()
        Ix, Iy = 0, 0
        Qlia.lock()
        Qx, Qy = Qlia.get_xy()
        Qlia.unlock()
        t = time.time()
        Is.append(Ix + 1j*Iy)
        Qs.append(Qx + 1j*Qy)
        Ts.append(T3)
        ts.append(t - t0)
    
    Is = np.array(Is)
    Qs = np.array(Qs)
    Ts = np.array(Ts)
    ts = np.array(ts)
    
    output = {'freq': frequencies, 'I': Is, 'Q': Qs, 'T3': Ts, 'cavity': vna_sweep,
              'time': ts}
    np.save(os.path.join(data_dir, 'OMIT_{}.npy'.format(timestamp)), output)