# -*- coding: utf-8 -*-
"""
Created on Fri Jul 21

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
from glob import glob
from scipy.ndimage import gaussian_filter
from scipy.signal import savgol_filter

cmap = plt.get_cmap('viridis')

plt.close('all')


files = []

# data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T900/red/0'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T800/red/0'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T800/red/1'
# files += glob(path.join(data_dir, '*.npy'))

def build_resp(file):
    data = np.load(file, allow_pickle=True).item()
    Is = data['Is']
    Qs = data['Qs']
    resp = []
    for I, Q in zip(Is, Qs):
        resp.append(np.mean(np.abs(I + 1j*Q)**2))
        # resp.append(abs(Q).mean())
    return np.array(resp)
    

data_dir = '../data/microwaves/RUN20201214/filled/driven/IQ/T800/DC/blue/1'
files += glob(path.join(data_dir, '*.npy'))

print(len(files))

fig1, ax1 = plt.subplots(1,1)
figall, axall = plt.subplots(1, 1)
Imean = {}
xmean = {}
ymean = {}
Ntm = {}
RFs = []

figrf, axrf = plt.subplots(1, 1)

colors = {900: 'k', 1000: 'r', 800: 'g', 700: 'b'}
avgT = 0
for k, fn in enumerate(files):
    print(fn)
    file = np.load(fn, allow_pickle=True).item()
    vna_sweep = file['vna_sweep']
    f = vna_sweep[:,0]
    S12 = np.sqrt(vna_sweep[:,1]**2 + vna_sweep[:,2]**2)
    # S12 = vna_sweep[:,1]
    
    T = file['temperatures'][-1][1]
    avgT += T
    
    Tmk = int(round(T*10))*100
    if Tmk in colors:
        color = colors[Tmk]
    else:
        color=None
        
    axrf.plot(f, S12, color=cmap(k/len(files)))
    axrf.axvline(file['RF_freq'])

    freq = file['freqs']
    r = build_resp(fn)
    if 'drive_power_dbm' in file:
        amp = file['drive_power_dbm']
    else:
        amp = -10

    print(T, amp)
    pl = ax1.plot(freq, r, '-', alpha=0.5)
    # ax1.plot(freq, savgol_filter(r, 11, 0), '-', color=pl[-1].get_color())
    # ax1.plot(freq, gaussian_filter(r, 3), '-')#, color=pl[-1].get_color())
    bgp = np.polyfit(freq, r, deg=3)
    bg = np.polyval(bgp, freq)
    # bg = gaussian_filter(r, sigma=1)
    axall.plot(freq, r, '-o', color=color)
# for ax in axs:
#     ax.legend(loc='best')
avgT = avgT/len(files)
ax1.set_title('DC, {:.0f} mK'.format(avgT*1000))
axall.set_title("all harmonics, {:.0f} mK".format(avgT*1000))