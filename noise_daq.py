# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 16:50:03 2022

@author: emil
"""

import os
import time
import numpy as np
import pyvisa as visa

import sys
sys.path.append('C:/code/labtools')
import cryoutil as cu

sys.path.append('D:/ev')
from instruments.SR830 import SR830
from instruments.rfsource import RFsource
from instruments.DAQcard import DAQcard
from instruments.vna import VNA
from instruments.DS345 import DS345

def wait_for_T3(cc, T3max, was_warm):
    while True:
        try:
            T3_init = cu.get_T3(cc)
            if T3_init < T3max:
                if was_warm:
                    print("First cooldown, waiting 3 min")
                    was_warm = False
                    time.sleep(180)
                break
            print("T3 = {}".format(T3_init))
            was_warm = True
        except:
            print("Couldn't read T3, trying again.")
            time.sleep(1)
        time.sleep(5)
    return T3_init

base_output_dir = 'D:/ev/data/microwaves/RUN20220720/filled_full/425mbar/600mK/spring_noise/0dBm/am_lock/1'

rm = visa.ResourceManager(r"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\agbin\visa32.dll")
cc = cu.get_cryocon(rm)
Ilia = SR830(rm, 'GPIB0::1::INSTR')
lo = RFsource(rm, 'LOsource')
rf = RFsource(rm, 'RFsource')
vna = VNA(rm, 'VNA')
ref = DS345(rm, 'GPIB0::5::INSTR')

daq_rate = 100 #Hz
daq_samples = int(5*60 * daq_rate) # points, 5 minutes
max_T = 0.605

RFpower = 0 #dBm
LIAfreq = 1391 #Hz
LIAtc = '30m'
LIAsens = '10u'
LIAslope = '12'
ndaqs = 2


daq = DAQcard(channels=['ai0', 'ai1', 'ai2'], rate=daq_rate, samples=daq_samples)

Ilia.set_reference('external')
ref.frequency(LIAfreq)
ref.amplitude(0.01, 'VP')
ref.output(True)
# Ilia.set_frequency(LIAfreq)
Ilia.set_timeconstant(LIAtc)
Ilia.set_sensitivity(LIAsens)
Ilia.set_slope(LIAslope)
Ilia.harmonic(1)

vna.setup('S21')
vna.power(-20)

# RF_frequencies = np.linspace(2.30597e9 - 0.5e6, 2.30597e9 + 0.5e6, 20, endpoint=True)
fc = 2.30759e9 #2.30692e9
span = 1.5e6
Nfreq = 51
RF_frequencies = np.linspace(fc - span/2, fc + span/2, Nfreq, endpoint=True)

#shuffle the RF frequencies to avoid biasing the sweep
rng = np.random.default_rng()
rng.shuffle(RF_frequencies)

lo.output(True)
rf.output(True)
rf.power(RFpower)
was_warm = False
while True:
    for RF_freq in RF_frequencies:
        print("Starting RF f {:.2f}".format(RF_freq))
        data_dir = os.path.join(base_output_dir, 'RF_{:.0f}'.format(RF_freq))
        os.makedirs(data_dir, exist_ok=True)
        lo.lock()
        lo.frequency(RF_freq)
        lo.unlock()
        rf.frequency(RF_freq)
        
        wait_for_T3(cc, max_T, False)
        print("Taking vna sweep.")
        vna_sweep = vna.sweep_cs(fc, 10e6, num_points=1000, bw=200)
        vna.output_off()
        
        print("waiting 120 s for homdyne lock")
        time.sleep(120)
        
        k = 0
        while k < ndaqs:
            print(k)
            T3_init = wait_for_T3(cc, max_T, was_warm)
            was_warm = False
            
            # print("Taking vna sweep.")
            # vna_sweep = vna.sweep_cs(fc, 10e6, num_points=1000, bw=200)
            # vna.output_off()
        
            # print("waiting 120 s for homdyne lock")
            # time.sleep(120)
            
            print("Starting DAQ ({}/{})".format(k, ndaqs))
            timestamp = time.strftime("%Y%m%d-%H%M%S")
            data = daq.measure()
            print("...done")
            
            while True:
                try:
                    T3_final = cu.get_T3(cc)
                    break
                except:
                    print("Couldn't read T3, trying again.")
                    time.sleep(5)
            
            if T3_final < max_T:
                output = {'data': data, 'rate': daq_rate, 'samples': daq_samples,
                          'T3i': T3_init, 'T3f': T3_final,
                          'LIAtc': LIAtc, 'LIAsens': LIAsens, 'LIAslope': LIAslope, 'LIAfreq': LIAfreq,
                          'RFpower': RFpower, 'RFfreq': RF_freq,
                          'vna_sweep': vna_sweep}
                np.save(os.path.join(data_dir, 'OMIT_{}.npy'.format(timestamp)), output)
                k += 1
            else:
                was_warm = True