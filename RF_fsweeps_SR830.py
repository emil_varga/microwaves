# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: emil
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from tqdm import tqdm

import instruments.DS345 as DS345
import instruments.SR830 as SR830
import instruments.SG384 as SG384

import matplotlib.pyplot as plt

import ltwsclient as ltwsc
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

T3max = 0.55

base_dir = '../data/RUN_20200917/RFvariable/MECH85kHz'

freqs = [(75e3, 95e3, 2000, 0)]
amplitudes = [0.2]
RFfreq_cent = 2.7237e9 # Hz
RFfreq_span = 10e6 # Hz
RFfreq_N = 20
RFfreqs = np.linspace(RFfreq_cent - RFfreq_span/2, RFfreq_cent + RFfreq_span/2, RFfreq_N)


reps = 1000

rm = visa.ResourceManager()
lockin = SR830.SR830(rm, 'GPIB0::1::INSTR')
lockin.set_sensitivity('10u')
lockin.set_timeconstant('100m')
wait_time = 0.5 # s

generator = DS345.DS345(rm, 'GPIB0::2::INSTR')
rf = SG384.SG384(rm, 'GPIB0::27::INSTR')
rf.frequency(RFfreqs[0])

# plt.close('all')
# fig, ax = plt.subplots(1,1)
# i = 0
# zorder = 0  

cmap = plt.get_cmap('viridis')

def animate(i, ax, fs, xs, ys, amp):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i:
        ax.lines[i].set_xdata(fs)
        ax.lines[i].set_ydata(rs)
        ax.lines[i].set_zorder(5)
    else:
        ax.plot(fs, rs, '-o', ms=3, zorder=5, color=cmap(amp))
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    r = 0
    while r < reps:
        r += 1
        for RFfreq in RFfreqs:
            rf.frequency(RFfreq)
            output_dir = path.join(base_dir, 'RF_{:.0f}'.format(RFfreq))
            os.makedirs(output_dir,exist_ok=True)
            for A in amplitudes:
                for fr in freqs:
                    fs = np.linspace(fr[0], fr[1], fr[2])
                    timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    xs, ys = [], []
                    fs_r = []
                    
                    T3i = lt.wait_for_T3(T3max)
                    print("Starting {} Hz, {} V".format(fr, A))
    
                    generator.amplitude(A, unit="VR")
                    generator.frequency(fs[0]) # set the first frequency
                    fstr = generator.frequency()
                    time.sleep(1)
                    for freq in tqdm(fs):
                        generator.frequency(freq)
                        time.sleep(wait_time)
                        x, y = lockin.get_xy()
                        fstr = generator.frequency()
                        fs_r.append(float(fstr))
                        xs.append(x)
                        ys.append(y)
                    T3f = lt.get_T3()
                    data = np.column_stack((np.array(fs_r), xs, ys))
                    print(data.shape)
                    out = {'amplitude_rms': A, 'data': data, 'RF_freq': RFfreq,
                           'T3init': T3i, 'T3final': T3f}
                    np.save(path.join(output_dir, 'FS_SR_'+timestamp), out, allow_pickle=True)
finally:
    lockin.close()
    generator.amplitude(0)
    generator.close()