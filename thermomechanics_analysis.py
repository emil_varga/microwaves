# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
import scipy.signal as sig

import os
from glob import glob

def get_mean_resp(data_dir, fdemod=None):
    files = glob(os.path.join(data_dir, 'TM*.npy'))
    
    meas = np.load(files[0], allow_pickle=True).item()
    nperseg = int(meas['rate']/0.5)
    
    resp = 0
    for file in files:
        meas = np.load(file, allow_pickle=True).item()
        data = meas['timeseries']
        cdata = data[0, :] + 1j*data[1,:]
        # cdata = cdata - np.mean(cdata)
        # cdata /= np.std(cdata)
        # print(data.shape)
        # fft = np.fft.fftshift(np.fft.fft(cdata, n=len(cdata)))
        # resp += np.abs(fft)**2
        f, r = sig.welch(cdata, fs=meas['rate'], nperseg=nperseg)
        resp += r
    resp /= len(files)
    if fdemod is None:
        fdemod = meas['fdemod']
    freqs, _ = sig.welch(cdata, fs=meas['rate'], nperseg=nperseg)
    return freqs + fdemod, resp

freqs, mean_resp_0 = get_mean_resp('../data/microwaves/RUN20220707/empty_direct_higham/thermomechanics/T0', fdemod=0)
freqs2, mean_resp_2 = get_mean_resp('../data/microwaves/RUN20220707/empty_direct_higham2/thermomechanics/T0', fdemod=0)
freqs3, mean_resp_3 = get_mean_resp('../data/microwaves/RUN20220707/empty_direct_higham_offpeak/thermomechanics/T0', fdemod=0)
# freqs2, mean_resp_2 = get_mean_resp('../data/microwaves/RUN20220707/empty_ampm2/thermomechanics/T0', fdemod=0)

plt.close('all')
fig, ax = plt.subplots(1, 1)
ax.semilogy(freqs, mean_resp_0)
ax.semilogy(freqs2, mean_resp_2)
ax.semilogy(freqs3, mean_resp_3)
# ax.semilogy(mean_resp_750)
# for A in allresp:
#     r = np.fft.fftshift(allresp[A][0])
#     ax.semilogy(freqs, r, label='A = {}'.format(A))
#     # ax.plot(freqs, savgol_filter(r, 5, 1), label='A = {}'.format(A))
# ax.legend(loc='best')
    