import numpy as np
import pyvisa as visa
import time
import os.path as path
import os

import sys
sys.path.append('D:/ev')
from instruments.rfsource import RFsource
from instruments.SR830 import SR830
from instruments.DS345 import DS345
from instruments.vna import VNA

sys.path.append('C:/code/labtools')
import cryoutil as cu

data_dir = r'D:\ev\data\microwaves\RUN20220720\filled_full\425mbar\600mK\OMIT\two_source\2'
os.makedirs(data_dir, exist_ok=True)

rm = visa.ResourceManager(r"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\agbin\visa32.dll")

lo = RFsource(rm, 'LOsource')
rf = RFsource(rm, 'RFsource')
gen = DS345(rm, 'GPIB0::5::INSTR')
lia = SR830(rm, 'GPIB0::1::INSTR')
vna = VNA(rm, 'VNA')

cc = cu.get_cryocon(rm)

T3max = 0.605 #K

pump_freqs = np.linspace(2.3072e9, 2.308e9, 5, endpoint=True)

vna_center = 2.307255335e9 #Hz
vna_span = 10e6 #Hz

f_pump = vna_center + 1392 #Hz

df_i = 1392.1 #Hz
df_f = 1393.1 #Hz
Ndf = 250

probe_powers = [-15, -10] #dBm

LIAtc = '1'
LIAslope = '12'
LIAsens = '50m'
wait_time = 5

lia.set_timeconstant(LIAtc)
lia.set_slope(LIAslope)
lia.set_sensitivity(LIAsens)
lia.set_reference('external')
lia.phase(0)

vna.setup('S21')
vna.power(-20)

gen.amplitude(0.01, 'VP')
gen.output(True)
gen.frequency(df_i)

lo.frequency(f_pump)
rf.frequency(f_pump+df_i)
rf.am(False)
rf.pm(False)
rf.power(probe_powers[0])

lo.output(True)
rf.output(True)

dfs = np.linspace(df_i, df_f, Ndf, endpoint=True)
was_warm = False
while True:
    for pump_freq in pump_freqs:
        data_dir_rf = path.join(data_dir, 'pump_f_{:.3f}'.format(pump_freq))
        os.makedirs(data_dir_rf, exist_ok=True)
        lo.frequency(pump_freq)
        rf.frequency(pump_freq+df_i)
        for probe_power in probe_powers:
            rf.power(probe_power)
            z = []
            T3s = []
            while True:
                try:
                    T3 = cu.get_T3(cc)
                    if T3 < T3max:
                        if was_warm:
                            print("First cooldown, waiting 3 min")
                            was_warm = False
                            time.sleep(180)
                        break
                    print("T3 = {}".format(T3))
                    was_warm = True
                except:
                    print("Couldn't read T3, trying again.")
                time.sleep(5)
            vna_sweep = vna.sweep_cs(vna_center, vna_span, num_points=1000, bw=200)
            vna.output_off()
            for df in dfs:
                rf.frequency(pump_freq + df)
                gen.frequency(df)
                print("Starting {:.3f}".format(df))
                while True:
                    try:
                        T3 = cu.get_T3(cc)
                        if T3 < T3max:
                            if was_warm:
                                print("First cooldown, waiting 3 min")
                                was_warm = False
                                time.sleep(180)
                            break
                        print("T3 = {}".format(T3))
                        was_warm = True
                    except:
                        print("Couldn't read T3, trying again.")
                    time.sleep(5)
                time.sleep(wait_time)
                x, y = lia.get_xy()
                z.append(x + 1j*y)
                T3s.append(T3)
            
            timestamp = time.strftime("%Y%m%d-%H%M%S")
            output = {'data': np.array(z), 'freq': dfs, 'f_pump': pump_freq, 'probe_power': probe_power,
                      'LIAtc': LIAtc, 'LIAslope': LIAslope, 'LIAsens': LIAsens, 'T3s': np.array(T3s),
                      'vna_sweep': vna_sweep, 'direction': -1}
            
            np.save(path.join(data_dir_rf, 'OMIT2_{}.npy'.format(timestamp)), output)
