# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 17:20:32 2020

@author: Davis
"""


import numpy as np
import visa
import time

import sys
sys.path.append('D:\\ev')
from instruments.SR830 import SR830
from instruments.SG384 import SG384

if __name__ == '__main__':
    rm = visa.ResourceManager()
    lockin = SR830(rm, 'GPIB0::8::INSTR')
    lo = SG384(rm, 'GPIB0::3::INSTR')
    
    P = 10
    I = 5
    integral = 0
    update_dt = 0.3
    values = []
    last_update = time.time()
    try:
        while True:
            lockin.lock(timeout=10000)
            x = lockin.get_aux(1)
            lockin.unlock()
            
            t = time.time()
            values.append(x)
            if t - last_update > update_dt:
                imbalance = np.mean(values)
                integral += imbalance*update_dt*100
                phase = P*imbalance + I*integral
                if phase > 360 or phase < -360:
                    phase = 0
                    integral = 0
                
                lo.lock()
                lo.phase(phase)
                print("{:.1f} {:.0f}, {:+.3f}, {}".format(phase, lo.phase(), imbalance, len(values)))
                lo.unlock()
                last_update = t
                values = []
    finally:
        if lockin.locked:
            lockin.unlock()
        lockin.close()
        lo.close()
        rm.close()