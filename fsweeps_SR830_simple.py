# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: emil
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from tqdm import tqdm

import sys
sys.path.append("..")
import instruments.SR830 as SR830
from instruments.KS33210A import KS33210A

import matplotlib.pyplot as plt


output_dir = '../data/RUN_20201124/lockin-sweep/long/2'

freqs = [(500, 7500, 60000, 0)]
amplitudes = [8, 5, 2]
reps = np.inf

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()
lockin = SR830.SR830(rm, 'GPIB0::1::INSTR')
lockin.set_reference('external')
lockin.set_sensitivity('200u')
lockin.set_timeconstant('30m')
lockin.harmonic(2)

gen = KS33210A(rm, '33210A')
# gen.lolevel(0)
# gen.hilevel(0.1)
gen.amplitude(0.1)
gen.output(True)
wait_time = 0.03 # s

# plt.close('all')
fig, ax = plt.subplots(1,1)
i = 0
zorder = 0  

cmap = plt.get_cmap('jet')

def animate(i, ax, fs, xs, ys, amp):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i+1:
        ax.lines[i+1].set_xdata(fs)
        ax.lines[i+1].set_ydata(rs)
        ax.lines[i+1].set_zorder(5)
    else:
        ax.plot(fs, rs, '-o', ms=3, zorder=5, color=cmap(amp))
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    r = 0
    while r < reps:
        r += 1
        for A in amplitudes:
            # lockin.set_output_amplitude(A)
            # gen.hilevel(A)
            gen.amplitude(0.1)
            for fr in freqs:
                fs = np.linspace(fr[0], fr[1], fr[2])
                timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                xs, ys = [], []
                fs_r = []
                print("Starting {} Hz, {} V".format(fr, A))
                gen.frequency(fs[0])
                time.sleep(5)
                for freq in tqdm(fs):
                    gen.frequency(freq)
                    time.sleep(wait_time)
                    x, y = lockin.get_xy()
                    fs_r.append(freq)
                    xs.append(x)
                    ys.append(y)
                    animate(i, ax, fs_r, xs, ys, A/8)
                data = np.column_stack((np.array(fs_r), xs, ys))
                print(data.shape)
                out = {'amplitude_rms': A, 'data': data}
                np.save(path.join(output_dir, 'FS_SR_'+timestamp), out, allow_pickle=True)
                i+=1
                if i > 10:
                    i=0
finally:
    gen.output(False)
    gen.close()
    lockin.close()